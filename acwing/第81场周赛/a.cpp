#include <bits/stdc++.h>
using namespace std;

void solve() {
    int res = 0;
    int n;
    cin >> n;
    for (int i = 0; i < n; i++) {
        double a, b, c;
        cin >> a >> b >> c;
        if (a + b + c >= 2) {
            res++;
        }
    }
    cout << res << endl;
}

int main() {
    int t = 1;
    // cin >> t;
    while (t--) solve();
    return 0;
}