#include <bits/stdc++.h>
using namespace std;

// 递推或者 DP 思路
// https://www.acwing.com/solution/content/154204/
// https://www.acwing.com/activity/content/code/content/4928155/
/*
f[i] 表示把前 i 张牌，从牌堆顶开始的 i 张牌全部变成红色需要的轮数
前 i - 1 张都是红, 第 i 张是蓝色的时候, 那么此时将第i张反转, 然后前i-1全部变成蓝色, 消耗一轮
前i-1变红需要 f[i-1], 消耗1轮后, 然后还需要把前 i - 1 个蓝的变回来, 需要 f[i-1]
所以 f[i] = f[i-1] + 1 + f[i-1] = 2 * f[i-1] + 1
预处理 f[i] 之后, 每遇到一个蓝色, 答案加上 f[i-1] + 1 就行了
*/

const int N = 55;
int n;
long long f[N];
char s[N];

void solve() {
    cin >> n;
    cin >> s + 1;
    for (int i = 1; i <= n; i++) f[i] = f[i - 1] * 2 + 1;
    long long res = 0;
    for (int i = 1; s[i]; i++) {
        if (s[i] == 'B') {
            res += f[i - 1] + 1;
        }
    }
    cout << res << endl;
}

int main() {
    int t = 1;
    // cin >> t;
    while (t--) solve();
    return 0;
}