#include <bits/stdc++.h>
using namespace std;

// 这个思路比较妙
// 至少肯定有一个位置不会变
// 那么枚举所有位置，假设当前位置的数不变，需要的操作次数
// 最少的操作的情况肯定属于这个枚举的子集里面
// 这个思路没有违反充分必要性，因为一个数一旦固定，公差固定，整个数列就固定了

const int N = 1010;

int n, m;
int a[N];

int work(int k, bool flag = false) {
    int res = 0;
    for (int i = 1; i <= n; i++) {
        int b = a[k] + (i - k) * m;

        if (b <= 0) return n;
        if (b != a[i]) {
            res++;
            if (flag) {
                if (b > a[i])
                    printf("+ %d %d\n", i, b - a[i]);
                else
                    printf("- %d %d\n", i, a[i] - b);
            }
        }
    }

    return res;
}

int main() {
    cin >> n >> m;
    for (int i = 1; i <= n; i++) cin >> a[i];

    int res = n;
    for (int i = 1; i <= n; i++)
        res = min(res, work(i));

    cout << res << endl;
    for (int i = 1; i <= n; i++)
        if (work(i) == res) {
            work(i, true);
            break;
        }

    return 0;
}