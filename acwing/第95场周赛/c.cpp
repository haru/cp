#include <bits/stdc++.h>
using namespace std;

// 如果第一个数 x 是最小数就是必败
// 因为对手始终可以把 x - 1 丢给你当成第一个数
// 当第一个数是 0 时候你就输了

int main() {
    int t;
    cin >> t;
    while (t--) {
        int n;
        cin >> n;
        int x;
        cin >> x;
        bool first_min = true;
        for (int i = 2; i <= n; i++) {
            int y;
            cin >> y;
            if (y < x) first_min = false;
        }
        if (first_min)
            puts("Bob");
        else
            puts("Alice");
    }
}