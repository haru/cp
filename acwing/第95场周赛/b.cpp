#include <bits/stdc++.h>
using namespace std;
using ll = long long;

// 1 和本身已经是 2 个约数了

const int N = 1e6 + 10;
int primes[N], cnt;
bool st[N];

void get_primes(int n) {
    st[1] = true;  // 特判
    for (int i = 2; i <= n; i++) {
        if (!st[i]) primes[cnt++] = i;
        for (int j = 0; primes[j] * i <= n; j++) {
            st[primes[j] * i] = true;
            if (i % primes[j] == 0) break;
        }
    }
}

int main() {
    get_primes(N - 1);
    int n;
    cin >> n;
    while (n--) {
        ll x;
        cin >> x;
        ll r = sqrt(x);
        if (r * r == x && !st[r])
            puts("YES");
        else
            puts("NO");
    }
    return 0;
}