#include <bits/stdc++.h>
using namespace std;

const int N = 55;
int n, m, p[N];

int find(int x) {
    if (x != p[x]) p[x] = find(p[x]);
    return p[x];
}

int main() {
    cin >> n >> m;
    int cnt = n;
    for (int i = 1; i <= n; i++) p[i] = i;
    while (m--) {
        int a, b;
        cin >> a >> b;
        a = find(a), b = find(b);
        if (a != b) p[a] = b, cnt--;
    }
    cout << (1ll << n - cnt) << endl;
    return 0;
}