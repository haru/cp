#include <algorithm>
#include <cstring>
#include <iostream>
#include <vector>

//二分 + 抽屉原理
//还是有难度的...
// 这个代码同时可以处理掉 m <= n - 1 的情况
// 因为当 m <= n - 1 的时候，在选前 n 列的时候，所有的行已被全搞定。后面的都是必然重复前面选过的行

using namespace std;

const int N = 100010;

int n, m;
vector<int> g[N];
bool st[N];

bool check(int mid) {
    for (int i = 0; i < m; i++) st[i] = false;

    bool same = false;
    for (int i = 0; i < n; i++) {
        bool success = false;
        for (int j = 0; j < m; j++)
            if (g[j][i] >= mid) {
                success = true;
                if (st[j]) same = true;
                st[j] = true;
            }
        if (!success) return false;
    }

    return same;
}

int main() {
    int T;
    scanf("%d", &T);

    while (T--) {
        scanf("%d%d", &m, &n);
        for (int i = 0; i < m; i++) {
            g[i].resize(n);
            for (int j = 0; j < n; j++)
                scanf("%d", &g[i][j]);
        }

        int l = 1, r = 1e9;
        while (l < r) {
            int mid = l + r + 1 >> 1;
            if (check(mid))
                l = mid;
            else
                r = mid - 1;
        }

        printf("%d\n", r);
    }

    return 0;
}
