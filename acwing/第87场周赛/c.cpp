#include <algorithm>
#include <array>
#include <bitset>
#include <cassert>
#include <cmath>
#include <cstring>
#include <deque>
#include <iostream>
#include <map>
#include <queue>
#include <random>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define um unordered_map
#define pq priority_queue
#define sz(x) ((int)(x).size())
#define fi first
#define se second
#define endl '\n'
typedef vector<int> vi;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
mt19937 mrand(random_device{}());
const ll mod = 1000000007;
int rnd(int x) { return mrand() % x;}
ll mulmod(ll a, ll b) {ll res = 0; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = (res + a) % mod; a = 2 * a % mod;} return res;}
ll powmod(ll a, ll b) {ll res = 1; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = res * a % mod; a = a * a % mod;} return res;}
ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a;}
//head

// DFS 求树的直径
//
const int N = 1e5 + 10, M = 2 * N;
int h[N], e[M], ne[M], idx;
int ans;
int n, m;

void add(int a, int b){
  e[idx] = b, ne[idx] = h[a], h[a] = idx++;
}

int dfs(int u, int fa){
  int d1 = 0, d2 = 0; //最大 次大
  for(int i = h[u]; ~i; i = ne[i]){
    int j = e[i];
    if(j == fa)continue;
    int d = dfs(j, u) + 1;
    if(d >= d1) d2 = d1, d1 = d;
    else if(d > d2) d2 = d;
  }
  ans = max(ans, d1 + d2);
  return d1;
}

void run_case() {
  cin >> n >> m; 
  memset(h, -1, sizeof h);
  for(int i = 0; i < m; i++){
    int a,b ;
    cin >> a >> b;
    add(a,b), add(b,a);
  }
  dfs(1, -1);
  cout << ans << endl;
}

signed main() {
  ios::sync_with_stdio(false);
  cin.tie(nullptr);
  int tests = 1;
  // cin >> tests;
  while (tests-- > 0)
    run_case();
  return 0;
}

