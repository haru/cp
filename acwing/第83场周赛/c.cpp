#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define um unordered_map
#define pq priority_queue
#define sz(x) ((int)(x).size())
#define fi first
#define se second
#define endl '\n'
typedef vector<int> vi;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
mt19937 mrand(random_device{}());
const ll mod = 1000000007;
int rnd(int x) { return mrand() % x;}
ll mulmod(ll a, ll b) {ll res = 0; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = (res + a) % mod; a = 2 * a % mod;} return res;}
ll powmod(ll a, ll b) {ll res = 1; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = res * a % mod; a = a * a % mod;} return res;}
ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a;}
//head

//这题其实非常脑残。。。
//脑残之处在于数据范围，你看 m 最大只有 1e8, 所以题目这个 a[i] <= 1e9 这个条件是没有用的
//在这个数据范围之下构造一个 1,2,3...., d, 2d, 的序列就行了, d = m - (n/2 - 1)
//防止后面出现重复, 先把 d, 2d 作为开头两项，然后n-2项就是 d * 2 + 1, d * 2 + 2 .... 无论怎么构造每项都不可能超过 1e9

//要是 m 范围超过 1e9 的话这题就难了，约束条件需要考虑 a[i] <= 1e9

void solve() {
    int n, m;
    cin >> n >> m;
    if(n == 1){
        if(m == 0) puts("1");
        else puts("-1");
    }
    else if(m < n /2) puts("-1");
    else{
        int d = m - (n/2 - 1);
        cout << d << ' ' << 2 * d << ' ';
        for(int i = 1; i <= n - 2; i++){
            cout << d * 2 + i << ' ';
        }
    }
}

int main() {
    int t = 1;
    // cin >> t;
    while (t --) solve();
    return 0;
}