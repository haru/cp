#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define um unordered_map
#define pq priority_queue
#define sz(x) ((int)(x).size())
#define fi first
#define se second
#define endl '\n'
typedef vector<int> vi;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
mt19937 mrand(random_device{}());
const ll mod = 1000000007;
int rnd(int x) { return mrand() % x;}
ll mulmod(ll a, ll b) {ll res = 0; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = (res + a) % mod; a = 2 * a % mod;} return res;}
ll powmod(ll a, ll b) {ll res = 1; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = res * a % mod; a = a * a % mod;} return res;}
ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a;}
//head

//就是正常贪心就行
//特殊关卡降序排序
//注意题意：是分数高于才可以修改，而不是不能挑战当前关卡，分数不够就直接加上当前遇到的关卡分数就行了

const int N = 110;
int w[N];
int id[N];
bool st[N];

void solve() {
    int n, m;
    cin >> n >> m;
    for(int i = 1; i <= n; i++){
        cin >> w[i];
    }
    for(int i = 0; i< m; i++){
        cin >> id[i];
        st[id[i]] = true;
    }
    sort(id, id + m, [&](int a, int b){
        return w[a] > w[b];
    });
    ll res = 0;
    for(int i = 1; i <= n; i++) 
        if(!st[i]) res += w[i];
    for(int i = 0; i < m; i++){
        res = max(res * 2, res + w[id[i]]);
    }
    cout << res << endl;
}

int main() {
    int t = 1;
    // cin >> t;
    while (t --) solve();
    return 0;
}