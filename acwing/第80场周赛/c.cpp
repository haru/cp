#include <bits/stdc++.h>
using namespace std;

//DP 这个状态转移要是没做过相关类型的题应该不太能做出来
//f[i][j][k][u] 表示此状态用了 i 个白, j 个黑, 最后相同的颜色结尾段有 k 个白 0个黑，或者 0个白u个黑 的方案数
//判断连续的同色长度，始终在当前状态字符串的结尾维护

const int N = 110, M = 11, mod = 1e8;
int n1, n2, k1, k2;
int f[N][N][M][M];

void solve() {
    cin >> n1 >> n2 >> k1 >> k2;
    f[0][0][0][0] = 1; //全0也是一种方案
    for (int i = 0; i <= n1; i++)
        for (int j = 0; j <= n2; j++)
            for (int k = 0; k <= k1; k++)
                for (int u = 0; u <= k2; u++) {
                    int v = f[i][j][k][u];
                    if (i + 1 <= n1 && k + 1 <= k1) //结尾放一个白子
                        f[i + 1][j][k + 1][0] = (f[i + 1][j][k + 1][0] + v)  % mod;
                    if (j + 1 <= n2 && u + 1 <= k2) //结尾放一个黑子
                        f[i][j + 1][0][u + 1] = (f[i][j + 1][0][u + 1] + v) % mod;
                }
    int res = 0;
    for (int i =  0; i <= k1; i++)
        for (int j = 0; j <= k2; j++) {
            res = (res + f[n1][n2][i][j]) % mod;
        }
    cout << res << endl;

}

int main() {
    int t = 1;
    //cin >> t;
    while (t --) {
        solve();
    }
    return 0;
}
