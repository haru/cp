#include <bits/stdc++.h>
using namespace std;

//dfs暴搜就行了

string num, ans;

void dfs(string str, int u, int s4, int s7) { // 当前第u位, s4, s7 为个数
    if (u == num.size()) {
        if (str >= num && (ans.empty() || ans > str))
            ans = str;
        return;
    }
    if (s4 < num.size() / 2) dfs(str + '4', u + 1, s4 + 1, s7);
    if (s7 < num.size() / 2) dfs(str + '7', u + 1, s4, s7 + 1);
}

void solve() {
    cin >> num;;
    if (num.size() % 2) num = '0' + num;
    dfs("", 0, 0, 0);
    if (ans.empty()) {
        num = "00" + num; //9999 这种偶数情况，要加两位
        dfs("", 0, 0, 0);
    }
    cout << ans << endl;
}

int main() {
    int t = 1;
    //cin >> t;
    while (t --) {
        solve();
    }
    return 0;
}
