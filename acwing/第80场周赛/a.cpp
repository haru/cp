#include <bits/stdc++.h>
using namespace std;

void solve() {
    string pat = "aeiouAEIOUyY";
    set<char> S;
    for (auto t : pat) {
        S.insert(t);
    }
    string x;
    getline(cin, x);
    reverse(x.begin(), x.end());
    // cout << x << endl;
    for (int i = 0; i < x.size(); i++) {
        if (isalpha(x[i])) {
            // cout << x[i] << endl;
            if (S.count(x[i])) {
                puts("YES");
                return;
            }
            else {
                puts("NO");
                return;
            }
        }
    }
}

int main() {
    int t = 1;
    //cin >> t;
    while (t --) {
        solve();
    }
    return 0;
}
