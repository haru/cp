#include <algorithm>
#include <cstring>
#include <iostream>

using namespace std;
const int N = 100010;

// 就直接枚举一下就行了..

int n;
string str[N];

int main() {
    cin >> n;

    int k = 0;
    for (int i = 0; i < n; i++) {
        string s;
        cin >> s;
        str[i] = s;
        if (s == "0") {
            cout << 0 << endl;
            return 0;
        }

        int cnt = 0;
        for (auto c : s)
            if (c > '1')
                k = i;
            else if (c == '1')
                cnt++;
        if (cnt > 1) k = i;
    }

    cout << str[k];
    for (int i = 0; i < n; i++)
        if (i != k) cout << str[i].substr(1);

    return 0;
}