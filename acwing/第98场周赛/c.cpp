#include <iostream>
#include <vector>
using namespace std;

// 看末尾有多少个0，就是看2，5因子个数取min就行了
// 然后就是经典的求 N! 中质因子 p 的个数的公式 = (n/p + n/p^2 + n/p^3 +...)
// 显然由公式可知 对于 p = 2, 5, 2的个数更多，所以答案就是 5 的因子个数了,
// 复杂度O(N*logN) 这题有单调性，n 越大末尾 0越多，所以可以二分更优化到 O(N)

int f(int n, int p) {  // n! 中 p 的个数
    int res = 0;
    while (n) res += n / p, n /= p;
    return res;
}

void run_case() {
    int m;
    cin >> m;
    vector<int> res;
    for (int i = 1;; i++) {
        int t = f(i, 5);
        if (t > m)
            break;
        else if (t == m)
            res.push_back(i);
    }
    cout << res.size() << endl;
    for (auto x : res) cout << x << ' ';
}

signed main() {
    ios::sync_with_stdio(false);
    cin.tie(nullptr);
    int tests = 1;
    // cin >> tests;
    while (tests-- > 0) run_case();
    return 0;
}
