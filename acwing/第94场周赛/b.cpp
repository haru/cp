#include <algorithm>
#include <array>
#include <bitset>
#include <cassert>
#include <cmath>
#include <cstring>
#include <deque>
#include <iostream>
#include <map>
#include <queue>
#include <random>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define um unordered_map
#define pq priority_queue
#define sz(x) ((int)(x).size())
#define fi first
#define se second
#define endl '\n'
typedef vector<int> vi;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
mt19937 mrand(random_device{}());
const ll mod = 1000000007;
const int INF = 0x3f3f3f3f;
int rnd(int x) { return mrand() % x;}
ll mulmod(ll a, ll b) {ll res = 0; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = (res + a) % mod; a = 2 * a % mod;} return res;}
ll powmod(ll a, ll b) {ll res = 1; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = res * a % mod; a = a * a % mod;} return res;}
ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a;}
//head

//Dijkstra 修改一下
//有个很重要的贪心思路就是每个点肯定选最早能到的时刻，肯定是最优的
//假设10，12这两个时刻，即使10到了然后不能出发，最坏也肯定是等到12，和12一样优

#define x first
#define y second

const int N = 1e5 + 10, M = 2 * N;
int n, m;
int h[N], e[M], ne[M], w[M], idx;
int dist[N];
bool st[N];
vector<int> delay[N];

void add(int a, int b, int c){
    e[idx] = b, w[idx] = c, ne[idx] = h[a], h[a] = idx++;
}

int add_delay(int ver, int distance){
    for(int t : delay[ver])
        if(t == distance) distance ++;
        else if(t > distance) break;
    return distance;
}

void dijkstra(){
    priority_queue<pii, vector<pii>, greater<pii> > heap;
    heap.push({0,1});
    memset(dist, 0x3f, sizeof dist);
    dist[1] = 0;
    while(heap.size()){
        auto t = heap.top();
        heap.pop();
        int ver = t.y;
        if(st[ver]) continue;
        st[ver] = true;
        int distance = add_delay(ver, t.x);
        for(int i = h[ver]; ~i; i = ne[i]){
            int j = e[i];
            if(dist[j] > distance + w[i]){
                dist[j] = distance + w[i];
                heap.push({dist[j], j});
            }
        }
    }
}

void run_case() {
    cin >> n >> m;  
    memset(h, -1, sizeof h);
    while(m --){
        int a, b, c;
        cin >> a >> b >> c;
        add(a,b,c), add(b, a, c);
    }
    for(int i = 1; i <= n; i++){
        int cnt;
        cin >> cnt;
        delay[i].resize(cnt);
        for(int j = 0; j < cnt; j++) cin >> delay[i][j];
    }
    dijkstra();
    if(dist[n] == INF) puts("-1");
    else cout << dist[n] << endl;
}

signed main() {
    ios::sync_with_stdio(false);
    cin.tie(nullptr);
    int tests = 1;
    // cin >> tests;
    while (tests-- > 0)
        run_case();
    return 0;
}

