#include <algorithm>
#include <array>
#include <bitset>
#include <cassert>
#include <cmath>
#include <cstring>
#include <deque>
#include <iostream>
#include <map>
#include <queue>
#include <random>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define um unordered_map
#define pq priority_queue
#define sz(x) ((int)(x).size())
#define fi first
#define se second
#define endl '\n'
typedef vector<int> vi;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
mt19937 mrand(random_device{}());
const ll mod = 1000000007;
int rnd(int x) { return mrand() % x;}
ll mulmod(ll a, ll b) {ll res = 0; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = (res + a) % mod; a = 2 * a % mod;} return res;}
ll powmod(ll a, ll b) {ll res = 1; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = res * a % mod; a = a * a % mod;} return res;}
ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a;}
//head

const int N = 110;
int a[N][N], b[N][N];

//暴力就行
//https://www.acwing.com/solution/content/167522/

void run_case() {
  int m, n, sum;
  cin >> m >> n;
  for (int i = 1; i <= 110; i ++ )
    for (int j = 1; j <= 110; j ++ )
      a[i][j] = 1;
  for (int i = 1; i <= m; i ++ )
    for (int j = 1; j <= n; j ++ ) {
      cin >> b[i][j];
      if (b[i][j] == 0) {
        for (int k = 1; k <= n; k ++ )
          a[i][k] = 0;
        for (int k = 1; k <= m; k ++ )
          a[k][j] = 0;
      }
    }
  //上面求出满足条件一的 A
  for (int i = 1; i <= m; i ++ )
    for (int j = 1; j <= n; j ++ )
      if (b[i][j] == 1) {
        sum = 0;
        for (int k = 1; k <= n; k ++ )
          sum += a[i][k];
        for (int k = 1; k <= m; k ++ )
          sum += a[k][j];
        if (sum == 0) {
          cout << "NO";
          return ; //与条件二矛盾
        }
      }
  cout << "YES" << endl;
  for (int i = 1; i <= m; i ++ ) {
    for (int j = 1; j <= n; j ++ )
      cout << a[i][j] << " ";
    cout << endl;
  }
}

signed main() {
  ios::sync_with_stdio(false);
  cin.tie(nullptr);
  int tests = 1;
  // cin >> tests;
  while (tests-- > 0)
    run_case();
  return 0;
}

