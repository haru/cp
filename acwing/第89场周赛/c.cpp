#include <algorithm>
#include <array>
#include <bitset>
#include <cassert>
#include <cmath>
#include <cstring>
#include <deque>
#include <iostream>
#include <map>
#include <queue>
#include <random>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define um unordered_map
#define pq priority_queue
#define sz(x) ((int)(x).size())
#define fi first
#define se second
#define endl '\n'
typedef vector<int> vi;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
mt19937 mrand(random_device{}());
const ll mod = 1000000007;
int rnd(int x) { return mrand() % x;}
ll mulmod(ll a, ll b) {ll res = 0; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = (res + a) % mod; a = 2 * a % mod;} return res;}
ll powmod(ll a, ll b) {ll res = 1; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = res * a % mod; a = a * a % mod;} return res;}
ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a;}
//head

const int N = 1e7 + 10;
ll n, x, y;
ll f[N];

//线性 DP 简单题

void run_case() {
  cin >> n >> x >> y;
  for(int i = 1; i<N; i++) f[i] = 1e18;
  f[0] = 0;
  for(int i = 1; i< N; i++){
    f[i] = min(f[i], f[i-1] + x); //只加
    //偶数除了可以加之外还有可能就是 * 2
    //偶数用减法得到肯定不是最优的，仔细想想就知道了
    if(i % 2 == 0) f[i] = min(f[i], f[i/2] + y);
    //奇数除了加得到之外可以是某个较小的数 * 2  再减"1"得到
    //乘2再减去更多得到，肯定不是最优的
    else f[i] = min(f[i], f[(i + 1) / 2] + x + y);
  }
  cout << f[n] << endl;
}

signed main() {
  ios::sync_with_stdio(false);
  cin.tie(nullptr);
  int tests = 1;
  // cin >> tests;
  while (tests-- > 0)
    run_case();
  return 0;
}

