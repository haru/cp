#include <bits/stdc++.h>
using namespace std;

/*
考虑贪心，从左向右扫描每一个点：
若其可以向左扩展，那么就向左扩展，不影响右侧点
若其不可以向左扩展但是可以向右扩展，则通过至多影响右侧一个点，换来了这个点，向右扩展
否则不扩展
*/

int main() {
    int n;
    scanf("%d", &n);

    vector<pair<int, int>> a(n + 1);
    for (int i = 0; i < n; i++) scanf("%d%d", &a[i].first, &a[i].second);
    // first 表示原点，second表示扩展长度

    a[n].first = 2147483647;  // 由于最后一点没有右边界限制，所以设为inf

    int cnt = 0;                           // cnt表示扩展最大区间数量
    for (int i = 0, r = -2e9; i < n; i++)  // r 表示上一个点带来的限制
    {
        if (a[i].first - a[i].second > r)  // 向左扩展
        {
            cnt++;
            r = a[i].first;
        } else if (a[i].first + a[i].second < a[i + 1].first)  // 向右扩展
        {
            cnt++;
            r = a[i].first + a[i].second;
        } else
            r = a[i].first;  // 不扩展
    }

    printf("%d", cnt);

    return 0;
}