#include <algorithm>
#include <cstring>
#include <iostream>
#include <map>
#include <vector>

using namespace std;

typedef pair<int, int> PII;

map<int, int> S, B;

// map 模拟一下就好了

int main() {
    int n, m;
    cin >> n >> m;

    /* 对于相同的交易价格进行合并 */
    while (n--) {
        int a, b;
        char op[2];
        cin >> op >> a >> b;
        if (op[0] == 'S')
            S[a] += b;
        else
            B[a] += b;
    }

    /* 将所有S操作的数据全部放在数组p */
    vector<PII> p;
    for (auto &[t, v] : S) p.push_back({t, v});

    int pp = p.size();
    int idx;
    /* 输出交易价格最低的 m 条卖出记录,map是从小到大排列的，所以逆序输出 */
    /* 若所需输出的条数小于数组的长度,则全部输出 */
    if (m > p.size())
        idx = p.size() - 1;
    else  // 否则就输出m条数据
        idx = m - 1;
    for (int i = idx; i >= 0; i--)
        cout << 'S' << " " << p[i].first << " " << p[i].second << endl;

    vector<PII> x;
    for (auto &[t, v] : B) x.push_back({t, v});
    /* 输出交易价格最高的 m 条买入 */
    int xx = x.size();
    for (int i = x.size() - 1; i >= max(0, xx - m); i--)
        cout << 'B' << " " << x[i].first << " " << x[i].second << endl;
}