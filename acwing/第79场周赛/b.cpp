#include <bits/stdc++.h>
using namespace std;

//简单数学题
//https://www.acwing.com/solution/content/151726/

int main(){
    int n, sum = 5, t =1;
    cin >> n;
    while(n > sum){
        n -= sum;
        sum *= 2;
        t *= 2;
    }
    if(n == 0){
        cout << "e" << endl;
        return 0;
    }
    if(n % t == 0) n = n / t - 1;
    else n = n / t;
    cout << (char)(n + 'a');
}
