#include <bits/stdc++.h>
using namespace std;

int n, m;
string str;
int min_cost = 0x3f3f3f3f;
string ans;

void work(char x){
    string s = str;
    vector<int> p[19];
    for(int i = 0; i < n; i++){
        p[s[i] - x + 9].push_back(i);
    }
    int cnt = 0, cost = 0;
    for(int k = 0; cnt < m; k++){ // enumerate cost k
        for(int i = 0; i < p[9+k].size() && cnt < m; i++){
            cnt++;
            cost += k;
            s[p[9 + k][i]] = x;
        } 
        if(k){ //这里一定要判断，不然会把 s[i] = x 的 cnt 算 2 遍
            for(int i = p[9-k].size() - 1; i >= 0 && cnt < m; i--){
                cnt++;
                cost += k;
                s[p[9-k][i]] = x;
            }
        }
    }
    if(cost < min_cost || cost == min_cost && s < ans){
        min_cost = cost;
        ans = s;
    }
}

int main(){
    cin >> n >> m;
    cin >> str;
    for(int i = 0; i < 10; i++){
        work(i + '0');
    }
    cout << min_cost << endl;
    cout << ans << endl;
    return 0;
}
