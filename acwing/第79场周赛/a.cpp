#include <bits/stdc++.h>
using namespace std;

int main(){
    int n = 500;
    //vector<int> a(n);
    set<int> S;
    for(int i= 1; i <= n; i++){
        S.insert(i * (i + 1 ) / 2);
    }
    int x;
    cin >> x;
    if(S.count(x)){
        puts("YES");
    }
    else puts("NO");
}
