#include <algorithm>
#include <cstring>
#include <iostream>
#include <unordered_set>

using namespace std;

// 逆向推导 O(nm)

const int N = 200010, M = 11;

int n;
char str[N][M];

int main() {
    scanf("%d", &n);
    for (int i = 0; i < n; i++) scanf("%s", str[i]);

    unordered_set<string> hash;
    for (int i = n - 1; i >= 0; i--)
        if (!hash.count(str[i])) {
            puts(str[i]);
            hash.insert(str[i]);
        }

    return 0;
}