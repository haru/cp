#include <algorithm>
#include <cstring>
#include <iostream>

using namespace std;

const int N = 100010;

int n;
int w[N], cnt[N];

int main() {
    scanf("%d", &n);
    for (int i = 0; i < n; i++) scanf("%d", &w[i]);

    int res = 0;
    for (int i = 0, j = 0, s = 0; i < n; i++) {
        if (!cnt[w[i]]) s++;
        cnt[w[i]]++;

        while (s > 2) {
            cnt[w[j]]--;
            if (!cnt[w[j]]) s--;
            j++;
        }

        res = max(res, i - j + 1);
    }

    printf("%d\n", res);
    return 0;
}