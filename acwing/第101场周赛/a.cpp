#include <bits/stdc++.h>
using namespace std;

int main() {
    double a, b, c;
    cin >> a >> b >> c;
    double res1 = (-1 * b + sqrt(b * b - 4 * a * c)) / (2 * a);
    double res2 = (-1 * b - sqrt(b * b - 4 * a * c)) / (2 * a);
    cout << fixed << showpoint;
    cout << setprecision(6);
    cout << max(res1, res2) << endl;
    cout << min(res1, res2) << endl;
    return 0;
}