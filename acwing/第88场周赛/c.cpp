#include <algorithm>
#include <array>
#include <bitset>
#include <cassert>
#include <cmath>
#include <cstring>
#include <deque>
#include <iostream>
#include <map>
#include <queue>
#include <random>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define um unordered_map
#define pq priority_queue
#define sz(x) ((int)(x).size())
#define fi first
#define se second
#define endl '\n'
typedef vector<int> vi;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
mt19937 mrand(random_device{}());
const ll mod = 1000000007;
int rnd(int x) { return mrand() % x;}
ll mulmod(ll a, ll b) {ll res = 0; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = (res + a) % mod; a = 2 * a % mod;} return res;}
ll powmod(ll a, ll b) {ll res = 1; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = res * a % mod; a = a * a % mod;} return res;}
ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a;}
//head

// 状态机类型 DP 
// https://www.acwing.com/solution/content/163735/

const int N = 110;

int f[N][3],a[N];
int n;

void run_case() {
  cin >> n;
  for(int i = 1;i <= n;i++) cin >> a[i];

  for(int i = 1;i <= n;i++){
    f[i][0] = max(f[i - 1][0],max(f[i - 1][1],f[i - 1][2]));    //如果今天休息
    f[i][1] = f[i][0];        //为这两个选择先赋初值，因为之后可能会被访问到
    f[i][2] = f[i][0];

    //如果选择去图书馆或者健身房
    if(a[i] == 1 || a[i] == 3) f[i][1] = max(f[i - 1][1],max(f[i - 1][0],f[i - 1][2]) + 1);
    if(a[i] == 2 || a[i] == 3) f[i][2] = max(f[i - 1][2],max(f[i - 1][0],f[i - 1][1]) + 1);
  }

  int res = 0;
  for(int i = 0;i <= 2;i++) res = max(res,f[n][i]);
  res = n - res;
  cout << res << endl;
}

signed main() {
  ios::sync_with_stdio(false);
  cin.tie(nullptr);
  int tests = 1;
  // cin >> tests;
  while (tests-- > 0)
    run_case();
  return 0;
}

