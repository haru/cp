#include <algorithm>
#include <array>
#include <bitset>
#include <cassert>
#include <cmath>
#include <cstring>
#include <deque>
#include <iostream>
#include <map>
#include <queue>
#include <random>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define um unordered_map
#define pq priority_queue
#define sz(x) ((int)(x).size())
#define fi first
#define se second
#define endl '\n'
typedef vector<int> vi;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
mt19937 mrand(random_device{}());
const ll mod = 1000000007;
int rnd(int x) { return mrand() % x;}
ll mulmod(ll a, ll b) {ll res = 0; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = (res + a) % mod; a = 2 * a % mod;} return res;}
ll powmod(ll a, ll b) {ll res = 1; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = res * a % mod; a = a * a % mod;} return res;}
ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a;}
//head


//https://www.acwing.com/solution/content/166092/
/*
 * 只要角上的四个点可以互相到达，那么这个图一定满足“从任意交点出发可以到达任意其它交点”。
 * 因为如果四个顶点可以互相到达，那么就代表边缘上的点同样可以。
 * 而图上的任意中间点可以先顺着所处的边的方向走到边缘，然后通过边缘的四条边的直接抵达某个点，满足要求。
  * */
const int N = 25;
int n, m;
string g1, g2;
int cnt1[N], cnt2[N];

void run_case() {
  cin >> n >> m;
  cin >> g1 >> g2;
  for (int i = 0; i < n; i ++ ) {
    if (g1[i] == '>') cnt1[i + 1] = 1;
  }
  for (int i = 0; i < m; i ++ ){
    if (g2[i] == '^') cnt2[i + 1] = 1;
  }
  bool ok = true;

  if (cnt1[1] != cnt2[1]) ok = false;
  if (cnt1[1] == cnt2[m]) ok = false;
  if (cnt1[n] == cnt2[1]) ok = false;
  if (cnt1[n] != cnt2[m]) ok = false;
  if (ok) printf("YES\n");
  else printf("NO\n");
}

signed main() {
  ios::sync_with_stdio(false);
  cin.tie(nullptr);
  int tests = 1;
  // cin >> tests;
  while (tests-- > 0)
    run_case();
  return 0;
}

