#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define um unordered_map
#define pq priority_queue
#define sz(x) ((int)(x).size())
#define x first
#define y second
#define endl '\n'
#define watch(x) cerr << (#x) << " is " << (x) << endl
typedef vector<int> vi;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
mt19937 mrand(random_device{}());
const ll mod = 1000000007;
int rnd(int x) { return mrand() % x;}
ll mulmod(ll a, ll b) {ll res = 0; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = (res + a) % mod; a = 2 * a % mod;} return res;}
ll powmod(ll a, ll b) {ll res = 1; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = res * a % mod; a = a * a % mod;} return res;}
ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a;}
//head

//这个题当数学题来推公式 __gcd(n-1,m-1)+1 还是有难度的，分类讨论情况很多
//用 DSU 比较通用的解法,  但是还是要变换坐标分类讨论, 总之很麻烦。。。
//https://www.acwing.com/activity/content/problem/content/7785/

const int N = 4000010;

int n, m;
int p[N];

int get(int x, int y)
{
    if (x == 1) return y;
    if (y == m) return m + x - 1;
    if (x == n) return m + n - 1 + m - y;
    if (y == 1) return m + n - 1 + m - 1 + n - x;
    return 0;
}

int find(int x)
{
    if (p[x] != x) p[x] = find(p[x]);
    return p[x];
}

int merge(int x, int y)
{
    x = find(x), y = find(y);
    p[x] = y;
}

int main()
{
    cin >> n >> m;
    for (int i = 1; i <= 2 * (n + m) - 4; i ++ ) p[i] = i;

    if (n > m) swap(n, m);

    for (int i = 1; i <= m; i ++ )
    {
        if (i <= n)
        {
            merge(get(1, i), get(i, 1));
            merge(get(n, i), get(n - i + 1, 1));
        }
        else
        {
            merge(get(1, i), get(n, i - n + 1));
            merge(get(n, i), get(1, i - n + 1));
        }

        if (m - i + 1 <= n)
        {
            merge(get(1, i), get(m - i + 1, m));
            merge(get(n, i), get(n - m + i, m));
        }
        else
        {
            merge(get(1, i), get(n, i + n - 1));
            merge(get(n, i), get(1, i + n - 1));
        }
    }

    int res = 0;
    for (int i = 1; i <= 2 * (n + m) - 4; i ++ )
        if (p[i] == i)
            res ++ ;

    cout << res << endl;
    return 0;
}