#include <bits/stdc++.h>
#include <set>
using ll = long long;
using namespace std;

vector<ll> v;
set<ll> S;
map<ll, int> mp;

int main() {
	ll n, k;
	cin >> n >> k;
	for(int i = 0; i < n; i++) {
		ll  x;
		cin  >>  x;
		mp[x] ++;
		S.insert(x);
		v.push_back(x);
	}
	ll res = 0;
	sort(v.begin(), v.end());
	v.erase(unique(v.begin(), v.end()), v.end());
	if(k == 1) {
		int cnt = 0;
		for(auto [x, y] : mp) {
			if(y == 3) cnt++;
		}
		cout << cnt << endl;
		return 0;
	}
	for(auto t : v) {
		if(S.count(t) && S.count(t * k) && S.count(t * k * k))
			res += 1ll * mp[t] * mp[t *k] * mp[t *k * k];
		// cout <<res  <<endl;
	}
	cout << res  <<  endl;
}