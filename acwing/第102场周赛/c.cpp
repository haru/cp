#include <bits/stdc++.h>
using ll = long long;
using namespace std;

int n, k;
const int N = 2e5 + 10;
int w[N];

int main() {
	ios::sync_with_stdio(false); //这题卡IO
	cin.tie(0);
	cin >> n >> k;
	map<int, int> L, R;
	for(int i = 0; i < n; i++) {
		cin  >>  w[i];
		R[w[i]] ++;
	}
	ll res = 0;
	for(int i = 0; i < n; i++) {
		int x = w[i];
		R[x] --;
		if(x %k == 0 && abs(1ll * k * x) <= 1e9)
			res += 1ll * L[x/k] * R[k*x];
		L[x] ++;
	}
	cout << res << endl;
}