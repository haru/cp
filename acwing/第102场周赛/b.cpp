#include <iostream>
#include <set>
using namespace std;


set<int> S;

int main() {
	int n;
	cin >> n;

	while(n --) {
		int x;
		cin >> x;
		while(x % 2 == 0)  x/=2;
		while(x % 3 == 0)  x/=3;
		S.insert(x);
	}
	if(S.size() == 1) puts("Yes");
	else puts("No");
}