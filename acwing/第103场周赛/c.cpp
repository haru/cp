#include <bits/stdc++.h>
using namespace std;

//https://www.acwing.com/video/4723/

void solve() {
	int n;
	cin >> n;
	vector<int> res;
	for(int i = 2; i <= n; i++ ) {
		set<int> hash;
		int m = i;
		for(int j =2; j * j <= m; j++ )
			if(m % j == 0) {
				while(m % j==0) m/=j;
				hash.insert(j);
			}
		if(m > 1) hash.insert(m);
		if(hash.size() == 1) res.push_back(i); // i = p^k, p is prime
	}
	cout << res.size() << endl;
	for(auto t : res) cout << t << ' ';
	cout << endl;
}

int main() {
	int t = 1;
	//cin >> t;
	while(t --) solve();
	return 0;
}