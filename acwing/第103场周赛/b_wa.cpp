#include <bits/stdc++.h>
using namespace std;

const int N = 4010, M = 2 * N;

//int in[N], out[N];
int deg[N];
int g[N][N];

void solve() {
	int n, m;
	cin >> n >> m;
	for(int i= 0; i< m; i++) {
		int a,b ;
		cin >> a >> b;
		g[a][b] = g[b][a] = 1;
		deg[a]++, deg[b++];
	}
	sort(deg, deg + n, greater<int>());
	int cnt = deg[0] + deg[1] + deg[2];
	int res = 0;
	if(deg[0] < 2) res = -1;
	else res = m - cnt + 3;
	for(int i=  0; i <= 2; i++)
	cout << res << endl;
}

int main() {
	int t = 1;
	//cin >> t;
	while(t --) solve();
	return 0;
}