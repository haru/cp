#include <bits/stdc++.h>
using namespace std;

void solve() {
	int a, b, c;
	cin >> a >> b >> c;
	bool flag = false;

	for(int x = 0; x * a <= c; x++)
		if((c - a * x) % b == 0)
			flag = true;
	if(flag) puts("Yes");
	else puts("No");
}

int main() {
	int t = 1;
	cin >> t;
	while(t --) solve();
	return 0;
}