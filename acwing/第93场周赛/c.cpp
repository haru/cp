#include <bits/stdc++.h>
using namespace std;

//https://www.acwing.com/video/4644/
//树形 DP + trie

const int N = 3e6 + 10, INF = 2e9;

int n;
int tr[N][2], idx;

void insert(int x){
    int p = 0;
    for(int i = 29; i >= 0; i--){
        int u = x >> i & 1;
        if(!tr[p][u]) tr[p][u] = ++idx;
        p = tr[p][u];
    }
}

int dfs(int u, int d){
    if(d == -1) return 0;
    int f[2];
    for(int i = 0; i< 2; i++){
        int p = tr[u][i];
        if(p) f[i] = dfs(p, d-1);
        else f[i] = -1;
    }    
    int res = INF;
    for(int i = 0; i < 2; i++){ //我不是很懂这里。。
        int t = 0;
        for(int j = 0; j< 2; j++)
            if(f[j] != -1)
                t = max(t, f[j] + ((i ^ j) << d));
        res = min(res, t);
    }
    return res;
}

void solve(){
    cin >> n;
    while(n --){
        int x;
        cin >> x;
        insert(x);
    }
    cout << dfs(0, 29) << endl;
}

int main(){
    int t = 1;
    //cin >> t;
    while(t --) solve();
    return 0;
}