#include <bits/stdc++.h>
using namespace std;

void solve(){
    int n, k;
    cin >> n >> k;
    if(n < k) cout << k << endl;
    else cout << n - n % k + k << endl;
}

int main(){
    int t = 1;
    //cin >> t;
    while(t --) solve();
    return 0;
}