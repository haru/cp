#include <bits/stdc++.h>
using namespace std;

//dfs 有几个剪枝技巧
//https://www.acwing.com/video/4646/

using ll = long long;
const int INF = 1000;
int n;
int ans = INF;

void dfs(ll x, int d){
    bool st[10] = {0};
    int cnt = 0;
    for(ll y = x; y; y /= 10){
        cnt ++;
        st[y % 10] = true;
    }
    if(d + n - cnt >= ans) return; //剪枝
    if(cnt == n){
        ans = d;
        return;
    }
    for(int i = 9; i >= 2; i--)
        if(st[i]) dfs(x * i, d + 1);
}

void solve(){
    ll x;
    cin >> n >> x;      
    dfs(x, 0);
    if(ans == INF) ans = -1;
    cout << ans << endl;
}

int main(){
    int t = 1;
    //cin >> t;
    while(t --) solve();
    return 0;
}