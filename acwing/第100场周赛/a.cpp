#include <bits/stdc++.h>
using namespace std;

void solve() {
    int n, b, p;
    cin >> n >> b >> p;
    cout << (n - 1) * (2 * b + 1) << ' ' << n * p << endl;
}

int main() {
    int t = 1;
    // cin >> t;
    while (t--) solve();
}