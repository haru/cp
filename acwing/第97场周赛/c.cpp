#include <algorithm>
#include <array>
#include <bitset>
#include <cassert>
#include <cmath>
#include <cstring>
#include <deque>
#include <iostream>
#include <map>
#include <queue>
#include <random>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define um unordered_map
#define pq priority_queue
#define sz(x) ((int)(x).size())
#define fi first
#define se second
#define endl '\n'
typedef vector<int> vi;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
mt19937 mrand(random_device{}());
const ll mod = 1000000007;
int rnd(int x) { return mrand() % x;}
ll mulmod(ll a, ll b) {ll res = 0; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = (res + a) % mod; a = 2 * a % mod;} return res;}
ll powmod(ll a, ll b) {ll res = 1; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = res * a % mod; a = a * a % mod;} return res;}
ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a;}
//head

// dfs 维护一下就行了

const int N = 1e5 + 10, M = N * 2;
int h[N], w[N], e[M], ne[M], idx;
int n, m;

void add(int a, int b) {
  e[idx] = b, ne[idx] = h[a], h[a] = idx++;
}

//当前节点 父节点 root 到当前节点连续黑色数量 当前点是否有效
int dfs(int u, int fa, int cnt, bool valid) {
  if(w[u]) cnt++;
  else cnt = 0;

  if (cnt > m) valid = false;
  int res = 0, sons = 0;
  for(int i = h[u]; ~i; i = ne[i]) {
    int j = e[i];
    if(j == fa) continue;
    sons ++;
    res += dfs(j, u, cnt, valid);
  }
  if(!sons && valid) res++;
  return res;
}

void run_case() {
  cin >> n >> m;
  for(int i = 1; i <= n; i++) cin >> w[i];
  memset(h, -1, sizeof h);
  for(int i = 0; i < n - 1; i ++) {
    int a, b;
    cin >> a >> b;
    add(a, b), add(b, a);
  }
  cout << dfs(1, -1, 0, true) << endl;
}

signed main() {
  ios::sync_with_stdio(false);
  cin.tie(nullptr);
  int tests = 1;
  // cin >> tests;
  while (tests-- > 0)
    run_case();
  return 0;
}

