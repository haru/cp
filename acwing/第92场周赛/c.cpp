#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define um unordered_map
#define pq priority_queue
#define sz(x) ((int)(x).size())
#define fi first
#define se second
#define endl '\n'
#define watch(x) cerr << (#x) << " is " << (x) << endl
typedef vector<int> vi;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
mt19937 mrand(random_device{}());
const ll mod = 1000000007;
int rnd(int x) { return mrand() % x;}
ll mulmod(ll a, ll b) {ll res = 0; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = (res + a) % mod; a = 2 * a % mod;} return res;}
ll powmod(ll a, ll b) {ll res = 1; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = res * a % mod; a = a * a % mod;} return res;}
ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a;}
//head

//并查集+贪心
//这个题的难点在于要考虑到对于任意一个连通图，把这个连通图改为菊花图，依然符合题意
//也就是说总存在一个菊花图的构造方式来加边让图连通
//加边的顺序是无所谓的，只要连通就行，那么肯定菊花图最优
//对于一个需求 i，如果 (xi, yi) 已经在一个集合中连通，此时多出来的还没用掉的边为 cnt 条
//那么贪心考虑把这 cnt 条边全部用来合并其他的集合，而且选出最大的 cnt + 1 个来合并
//然后合并后的这个大集合同样改为菊花图，依然保持前面所有的连通性不变，而且肯定最优
//如果集合的数量 < cnt + 1 那么就全部合并起来就行了，剩余的多出来的边在菊花图的基础上随便连就行

//朴素每次都排序选 cnt+1 大的集合 复杂度 O(n^2 * log(n))
//如果用平衡树动态维护有序可以做到 O(n*log(n))

//题目限制了 d <= n-1 所以不会导致加入太多边出现重边，最多也只能是个树
//题意还有个坑点就是，每个问题都是独立的，意思就是每个问题只会限制连通性要求，
//上个问题加的边，下个问题就会清空重新从n个独立的点开始加

const int N = 1010;
int n, d;
int p[N], s[N];
int sz[N];

int find(int x) {
    if (p[x] != x) p[x] = find(p[x]);
    return p[x];
}

void solve() {
    cin >> n >> d;
    for (int i = 1; i <= n; i++) p[i] = i, s[i] = 1;
    int cnt = 0;
    for (int i = 1; i <= d; i++) {
        int x, y;
        cin >> x >> y;
        x = find(x), y = find(y);
        if (x != y) { //这个需求不能累积多余的边, 一条边必须用于连接两个不在同一集合中的两点
            s[y] += s[x];
            p[x] = y;
        }
        else cnt++;

        int tt = 0;
        for (int j = 1; j <= n; j++)
            if (find(j) == j)
                sz[tt++] = s[j]; //找出点数最多的那些集合，用多出来的边进行贪心合并

        sort(sz, sz + tt, greater<int>());
        int sum = 0;
        for (int j = 0; j < tt && j < cnt + 1; j++)
            sum += sz[j];
        cout << sum - 1 << endl;
    }
}

int main() {
    int t = 1;
    // cin >> t;
    while (t --) solve();
    return 0;
}