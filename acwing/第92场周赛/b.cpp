#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define um unordered_map
#define pq priority_queue
#define sz(x) ((int)(x).size())
#define fi first
#define se second
#define endl '\n'
#define watch(x) cerr << (#x) << " is " << (x) << endl
typedef vector<int> vi;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
mt19937 mrand(random_device{}());
const ll mod = 1000000007;
int rnd(int x) { return mrand() % x;}
ll mulmod(ll a, ll b) {ll res = 0; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = (res + a) % mod; a = 2 * a % mod;} return res;}
ll powmod(ll a, ll b) {ll res = 1; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = res * a % mod; a = a * a % mod;} return res;}
ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a;}
//head

//就是一个二叉树的前序遍历
//注意判断无解的情况

string ans;

bool dfs() {
    char str[5];
    if (scanf("%s", str) == -1) return false; //需要输入一个节点但凡是此时已经没有了，无解

    if (!strcmp(str, "pair")) {
        ans += str;
        ans += '<';
        if (!dfs()) return false; //左子树
        ans += ',';
        if (!dfs()) return false; //右子树
        ans += '>';
    }
    else ans += str;

    return true;
}

void solve() {
    scanf("%*d"); // n 没用
    if (dfs() && scanf("%*s") == -1) puts(ans.c_str()); //有解后不能有多余的冗余节点了
    else puts("Error occurred");
}

int main() {
    int t = 1;
    // cin >> t;
    while (t --) solve();
    return 0;
}