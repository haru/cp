#include <bits/stdc++.h>
using namespace std;

//分类讨论 O(1) 做法

int main(){
    int c, d, n, m, k;
    cin >> c >> d >> n >> m >> k;
    int cnt = n * m - k;
    if(cnt <= 0){
        puts("0");
        return 0;
    }
    cout << min({cnt * d, cnt / n * c + cnt % n * d, (cnt + n -1) / n * c}) << endl;
}