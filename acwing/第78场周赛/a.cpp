#include <bits/stdc++.h>
using namespace std;

set<pair<string, string>> S;

int main(){
    int n;
    cin >> n;
    for(int i = 0; i< n; i++){
        string a, b;
        cin >> a >> b;
        S.insert({a,b});
    }
    cout << S.size() << endl;
    return 0;
}
