#include <bits/stdc++.h>
using namespace std;

//单调栈 + 二分
//https://www.bilibili.com/video/BV1ig411v7w8

const int N = 1e5 + 10;
int n;
int w[N], stk[N];
int ans[N];

int main(){
    cin >> n;
    for(int i = 0; i < n; i++) cin >> w[i]; 
    int top = 0;
    for(int i = n -1; i >= 0; i --){
        if(!top || w[i] <= w[stk[top]]) ans[i] = -1;
        else{
            // binary search
            int l = 1, r = top;
            while(l < r){
                int mid = l + r >> 1;
                if(w[stk[mid]] < w[i]) r = mid;
                else l = mid + 1;
            }
            ans[i] = stk[r] - i - 1;
        }
        if(!top || w[i] < w[stk[top]]) stk[++top] = i;
    }
    for(int i = 0; i < n; i++){
        cout << ans[i] << ' ';
    }
    //栈中元素是单调的
    //puts("");
    //for(int i = top; i >= 1; i--) cout << w[stk[i]] << ' ';
    return 0;
}
