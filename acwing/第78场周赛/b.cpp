#include <bits/stdc++.h>
using namespace std;

stack<char> st;

int main(){
    string s;
    cin >> s;
    for(int i = 0; s[i]; i++){
        if(!st.size()){
            st.push(s[i]);
        }
        else if(s[i] == st.top()){
            st.pop();
        }
        else{
            st.push(s[i]);
        }
    }
    string res;
    while(st.size()){
        res += st.top();
        st.pop();
    }
    reverse(res.begin(), res.end());
    cout << res << endl;
    return 0;
}
