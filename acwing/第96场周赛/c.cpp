#include <algorithm>
#include <array>
#include <bitset>
#include <cassert>
#include <cmath>
#include <cstring>
#include <deque>
#include <iostream>
#include <map>
#include <queue>
#include <random>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define um unordered_map
#define pq priority_queue
#define sz(x) ((int)(x).size())
#define fi first
#define se second
#define endl '\n'
typedef vector<int> vi;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
mt19937 mrand(random_device{}());
const ll mod = 1000000007;
int rnd(int x) { return mrand() % x;}
ll mulmod(ll a, ll b) {ll res = 0; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = (res + a) % mod; a = 2 * a % mod;} return res;}
ll powmod(ll a, ll b) {ll res = 1; a %= mod; assert(b >= 0); for (; b; b >>= 1) {if (b & 1)res = res * a % mod; a = a * a % mod;} return res;}
ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a;}
//head

// 树状数组裸题
// 维护两个BIT 维护一下 min(d[i], b), 以及维护 min(d[i], a)

const int N = 200010;
int n, k, a, b, q;
int d[N];
int tr1[N], tr2[N];

int lowbit(int x) {
    return x & -x;
}

void add(int tr[], int x, int v) {
    for(int i = x; i <= n; i += lowbit(i))
        tr[i] += v;
}

int query(int tr[], int x) {
    int sum = 0;
    for(int i = x; i; i -= lowbit(i))
        sum += tr[i];
    return sum;
}

void run_case() {
    cin >> n >> k >> a >> b >> q;
    while(q -- ){
        int t;
        cin >> t;
        if(t == 1) {
            int x, y;
            cin >> x >> y;
            add(tr1, x, min(d[x] + y, b) - min(d[x], b));
            add(tr2, x, min(d[x] + y, a) - min(d[x], a));
            d[x] += y;
        } else {
            int p;
            cin >> p;
            cout << query(tr1, p - 1) + query(tr2, n) - query(tr2, p + k -1) << endl;
        }
    }
}

signed main() {
  ios::sync_with_stdio(false);
  cin.tie(nullptr);
  int tests = 1;
  // cin >> tests;
  while (tests-- > 0)
    run_case();
  return 0;
}

