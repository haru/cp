#include <bits/stdc++.h>
using namespace std;

//https://www.acwing.com/video/4565/
//就是求出 m, n 不等式，如果满足就有解
//然后代码实现要一直判断后面是否满足 m > n-1 
//不然数组插入元素复杂度很高
//

#include <iostream>
#include <cstring>
#include <algorithm>

using namespace std;

int main()
{
    int n, m;
    scanf("%d%d", &n, &m);

    if (m < n - 1 || m > 2 * (n + 1)) puts("-1");
    else
    {
        for (int i = 0; i < 2; i ++ ) //开头尝试放
            if (m > n - 1)
            {
                printf("1");
                m -- ;
            }

        while (n)
        {
            printf("0");
            n -- ;
            if (n)
            {
                printf("1");
                m -- ;

                if (m > n - 1)
                {
                    printf("1");
                    m -- ;
                }
            }
        }

        while (m -- ) printf("1");
    }

    return 0;
}

