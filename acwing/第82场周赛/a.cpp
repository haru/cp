#include <bits/stdc++.h>
using namespace std;

void solve(){
    int n,k;
    cin >> n >> k;
    vector<int> a(n);
    for(auto &x : a) cin >> x;
    sort(a.begin(), a.end(), greater<int>());
    cout << a[k-1] << endl;
}

int main(){
    int t =1;
    //cin>>t;
    while(t --) solve();
    return 0;
}
