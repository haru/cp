syntax on
set hls
set is
set ts=4
set cin
set mouse=a
set smarttab
set expandtab
set noswapfile
set nu
set sw=4
set si
set ignorecase
inoremap { {}<Left>
inoremap {<CR> {<CR>}<Esc>O
"inoremap {{ {
"inoremap {} {}

