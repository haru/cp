class Solution {
public:
    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
        unordered_multiset<int> s;
        for(auto &x : nums1) s.insert(x);
        vector<int> res;
        for(auto &x : nums2) if(s.count(x)) {
            auto it = s.find(x);
            res.push_back(x);
            s.erase(it);
        }
        return res;
    }
};