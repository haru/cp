class Solution {
public:
    int combinationSum4(vector<int>& nums, int m) {
        int n = nums.size();
        vector<unsigned long long> f(m + 1);
        f[0] = 1;
        for(int j = 0; j <= m; j++)
            for(auto v : nums)
                if(j >= v) 
                    f[j] += f[j-v]; 
        return f[m];
    }
};

//顺序有关系，不是简单的完全背包求方案数
//f[j]: 总和为j的所有方案数