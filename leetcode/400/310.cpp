class Solution {
public:
    vector<int> findMinHeightTrees(int n, vector<vector<int>>& edges) {
        if(n == 1) return vector<int>{0};
        vector<vector<int>> g(n); 
        vector<int> d(n, 0);

        for(auto& e : edges) {
            int a = e[0], b = e[1];
            g[a].push_back(b), g[b].push_back(a);
            d[a] ++, d[b] ++;
        }

        vector<int> res;
        for(int i = 0; i < n; i++) if(d[i] == 1) res.push_back(i);

        while(n > 2) { // 最多只有2个点，想象一下三个点的情况，那么两点之间的那个点就会是唯一解
            vector<int> next_res;
            for(auto &u : res) {
                n --;
                for(auto&v : g[u]) {
                    d[v] --;
                    if(d[v] == 1) next_res.push_back(v);
                }
            }
            res = next_res;
        }

        return res;
    }
};

//贪心 O(n) 
/*
从叶子结点开始，每一轮删除所有叶子结点。
删除后，会出现新的叶子结点，此时再删除。
重复以上过程直到剩余 1 个或 2 个结点，此时这 1 个或 2 个结点就是答案。
*/

//也有树形DP的做法，也是O(n) 但是细节太多了