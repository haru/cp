class Solution {
public:
    int integerBreak(int n) {
        if(n <= 3) return n - 1;
        int p = 1;
        while(n >= 5) n -= 3, p *= 3;
        return p * n;
    }
};