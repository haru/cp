class Solution {
public:
    bool canMeasureWater(int a, int b, int c) {
        if(a + b < c) return false;
        if(c % __gcd(a, b) == 0) return true;
        return false;
    }
};

//bezout theory
//O(logn)