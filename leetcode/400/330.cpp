class Solution {
public:
    int minPatches(vector<int>& nums, int n) {
        long long x = 1;
        int res = 0, i = 0;
        while(x <= n) {
            if(i < nums.size() && nums[i] <= x) x += nums[i++];
            else x += x, res++;
        } 
        return res;
    }
};

//贪心
//当前能凑出 [0, x)
//x 表示下一个需要凑出来的数
//分类讨论 nums[i] <= x, nums[i] > x