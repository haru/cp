class Solution {
public:
    int lowbit(int x) {
        return x & -x;
    }
    bool isPowerOfFour(int n) {
        if(n > 0 && lowbit(n) == n && sqrt(n) * sqrt(n) == n) return true;
        return false;
    }
};

/*
1. n > 0
2. 只有因子2
3. 是平方数
*/