class Solution {
public:
    using ll = long long;
    unordered_map<ll, int> dp;
    int integerReplacement(int n) {
        return f(n); 
    }

    int f(ll n) {
        if(dp.count(n)) return dp[n];
        if(n == 1) return 0;
        if(n % 2 == 0) return dp[n] = 1 + f(n / 2);
        return dp[n] = min(f(n-1), f(n+1)) +1;
    }
};

//直接暴搜即可
//不用map记忆化 time: O(phi^log(n)) : https://leetcode.cn/problems/integer-replacement/solutions/1108099/zheng-shu-ti-huan-by-leetcode-solution-swef/
//记忆化 O(logn)