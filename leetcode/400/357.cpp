class Solution {
public:
    int countNumbersWithUniqueDigits(int n) {
        n = min(n, 10);
        if(!n) return 1;

        vector<int> f(n + 1);
        f[1] = 9; 
        f[0] = 1;
        long long res = f[1] + f[0];
        for(int i = 2; i <= n; i++) {
            f[i] = f[i-1] * (11 - i);
            res += f[i];       
        }
        return res;
    }
};