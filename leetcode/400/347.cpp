class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) {
        unordered_map<int, int> cnt;
        for(auto & t : nums) cnt[t]++;

        int n = nums.size();
        vector<int> s(n + 1);
        for(auto [x, c] : cnt) s[c]++;
        int i = n, t = 0;
        while(t < k) t += s[i--];

        vector<int> res;
        for(auto [x, c] : cnt)
            if(c > i) res.push_back(x);
        return res;
    }
};

//计数排序