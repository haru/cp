class Solution {
public:
    int wiggleMaxLength(vector<int>& nums) {
        nums.erase(unique(nums.begin(), nums.end()), nums.end()); 
        if(nums.size() <= 2) return nums.size();

        int res = 2; //首尾
        for(int i = 1; i < nums.size() - 1; i++) {
            int a = nums[i-1], b = nums[i], c = nums[i+1];
            if(b > a && b > c || b < a && b < c) res++;
        }
        return res;
    }
};

//贪心
//去掉连续的重复元素，取出所有局部极值即可