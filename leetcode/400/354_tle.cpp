class Solution {
public:
    int maxEnvelopes(vector<vector<int>>& w) {
        int n = w.size();
        vector<int> f(n);
        sort(w.begin(), w.end());
        int res = 0;
        for(int i = 0; i < n; i++) {
            f[i] = 1;
            for(int j = 0; j < i; j++) {
                if(w[j][0] < w[i][0] && w[j][1] < w[i][1])
                    f[i] = max(f[i], f[j] + 1);
            }
            res = max(res, f[i]);
        }
        return res;
    }
};

//排序 + LIS
//DP O(n^2) 过不了