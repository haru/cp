class Solution {
public:
    bool canConstruct(string a, string b) {
        unordered_map<char, int> hash;
        for(auto c : b) hash[c] ++;
        for(auto c : a) {
            if(!hash[c]) return false;
            hash[c] --;
        }       
        return true;
    }
};