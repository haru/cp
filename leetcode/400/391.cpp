class Solution {
public:
    bool isRectangleCover(vector<vector<int>>& r) {
        map<pair<int, int>, int> cnt;
        typedef long long LL;
        LL sum = 0;
        for (auto x: r) {
            LL a = x[0], b = x[1], c = x[2], d = x[3];
            ++ cnt[{a, b}], ++ cnt[{a, d}];
            ++ cnt[{c, b}], ++ cnt[{c, d}];
            sum += (c - a) * (d - b);
        }
        vector<vector<int>> res;
        for (auto& [x, y]: cnt)
            if (y == 1) res.push_back({x.first, x.second});
            else if (y == 3) return false;
            else if (y > 4) return false;
        if (res.size() != 4) return false;
        sort(res.begin(), res.end());
        return sum == (LL)(res[3][0] - res[0][0]) * (res[3][1] - res[0][1]);
    }
};
/*
1. 出现1次的点 4个
2. 出现3次的点 0个
3. 总面积之和与4个出现一次的点组成的矩形相同
*/