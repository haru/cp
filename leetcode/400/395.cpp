class Solution {
public:
    int K;
    unordered_map<char, int> cnt;

    void add(char &c, int &x, int& y) {
        if(!cnt[c]) x++;
        cnt[c]++;
        if(cnt[c] == K) y++;
    }

    void del(char &c, int & x, int& y) {
        if(cnt[c] == K) y--;
        cnt[c]--;
        if(!cnt[c]) x--;
    }

    int longestSubstring(string s, int _K) {
        K = _K;
        int res = 0;
        for(int k = 1; k <= 26; k++) {
            cnt.clear();
            for(int i = 0, j = 0, x = 0, y = 0; i < s.size(); i++) {
                add(s[i], x, y);
                while(x > k) del(s[j++], x, y);
                if(x == y) res = max(res, i - j + 1);
            }
        }
        return res;
    }
};

//限制条件加上, 使其有单调性的滑动窗口问题
//这题挺难的，因为最终的最优解一定是这种枚举方法的子集
//https://www.acwing.com/solution/content/163481/
