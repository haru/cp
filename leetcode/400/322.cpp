class Solution {
public:
    int coinChange(vector<int>& coins, int m) {
        vector<int> f(m + 1, 1e8);
        f[0] = 0; 
        for(auto v : coins)
            for(int j = v; j <= m; j++)
                f[j] = min(f[j], f[j-v] + 1);
        if(f[m] == 1e8) return -1;
        return f[m];
    }
};
//完全背包模板
//装满背包的情况下，最小的总价值
//每个硬币的面额为物品体积，价值都是1
//凑成的总体积就是面值和