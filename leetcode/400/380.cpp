class RandomizedSet {
public:
    unordered_map<int, int> hash;
    vector<int> nums;
    RandomizedSet() {
        
    }
    
    bool insert(int val) {
        if(hash.count(val)) return false;
        nums.push_back(val);
        hash[val] = nums.size()-1; 
        return true;
    }
    
    bool remove(int x) {
        if(!hash.count(x)) return false;
        int y = nums.back();
        int px = hash[x], py = hash[y];
        swap(nums[px], nums[py]);
        swap(hash[x], hash[y]);
        nums.pop_back();
        hash.erase(x);
        return true;
    }
    
    int getRandom() {
        return nums[rand() % nums.size()];  
    }
};

//哈希表 + 数组

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet* obj = new RandomizedSet();
 * bool param_1 = obj->insert(val);
 * bool param_2 = obj->remove(val);
 * int param_3 = obj->getRandom();
 */