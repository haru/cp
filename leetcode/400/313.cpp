class Solution {
public:
    int nthSuperUglyNumber(int n, vector<int>& primes) {
        typedef pair<long, long> PII;
        priority_queue<PII, vector<PII>, greater<PII>> heap;
        for (int x: primes) heap.push({x, 0});
        vector<int> q(n);
        q[0] = 1;
        for (int i = 1; i < n;) {
            auto t = heap.top(); heap.pop();
            if (t.first != q[i - 1]) q[i ++ ] = t.first;
            int idx = t.second, p = t.first / q[idx];
            heap.push({(long long)p * q[idx + 1], idx + 1});
        }
        return q[n - 1];
    }
};