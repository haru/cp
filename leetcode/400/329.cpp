class Solution {
public:
    vector<vector<int>> f, w;
    int n, m;
    int dx[4] = {-1, 0, 1, 0}, dy[4] = {0, 1, 0, -1};

    int longestIncreasingPath(vector<vector<int>>& matrix) {
        if(matrix.empty() || matrix[0].empty()) return 0;
        w = matrix;
        n = matrix.size(), m = matrix[0].size();
        f = vector<vector<int>>(n, vector<int>(m, -1));
        int res = 0;
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                res = max(res, dp(i, j));     

        return res;
    }

    int dp(int x, int y) {
        auto &v = f[x][y];
        if(v!=-1) return v;
        v = 1;
        for(int i = 0; i < 4; i++) {
            int a = x + dx[i], b = y + dy[i];
            if(a >= 0 && a < n && b >= 0 && b < m && w[x][y] < w[a][b])
                v = max(v, dp(a, b) + 1);
        }
        return v;
    }
};
//滑雪 记忆化搜索