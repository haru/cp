class Solution {
public:
    string reverseVowels(string s) {
        set<char> S;
        S.insert('a');
        S.insert('e');
        S.insert('i');
        S.insert('o');
        S.insert('u');

        for(int i = 0, j = s.size(); i < j;) {
            if(S.count(tolower(s[i])) && S.count(tolower(s[j]))) {
                swap(s[i], s[j]);
                i++, j--;
            }
            else if(S.count(tolower(s[i]))) j--;
            else i++;
        }
        return s;
    }
};