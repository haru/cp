class Solution {
public:
    vector<vector<int>> kSmallestPairs(vector<int>& a, vector<int>& b, int k) {
        if(a.empty() || b.empty()) return {};
        int n  = a.size(), m = b.size();
        priority_queue<vector<int>, vector<vector<int>>, greater<vector<int>> > heap;
        for(int i = 0; i < m; i++) heap.push({b[i] + a[0], 0, i});
        vector<vector<int>> res;

        while(k -- && heap.size()) {
            auto t = heap.top();
            heap.pop();
            res.push_back({a[t[1]], b[t[2]]});
            if(t[1] + 1 < n)
                heap.push({a[t[1] + 1] + b[t[2]], t[1] + 1, t[2]});
        } 
        return res;
    }
};

//感觉是贪心的思路，不是啥多路归并