/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
	ListNode* head;
    Solution(ListNode* _head) {
       	head = _head; 
    }
    
    int getRandom() {
    	int c = -1, n = 0;
    	for(auto p = head; p; p = p->next) {
    		n++;
    		if(rand() % n == 0) c = p->val;
    	} 
    	return c;
    }
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution* obj = new Solution(head);
 * int param_1 = obj->getRandom();
 */

//https://www.acwing.com/solution/content/14526/
//蓄水池抽样算法