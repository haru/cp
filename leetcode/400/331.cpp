class Solution {
public:
    string s;
    int k;
    bool isValidSerialization(string _s) {
        k = 0;
        s = _s;
        s += ',';
        if(!dfs()) return false;
        return k == s.size(); 
    }

    bool dfs() {
        if(k == s.size()) return false; //结尾肯定是 '#,' 到不了最后的 ','
        if(s[k] == '#'){
            k += 2;
            return true;
        }
        while(s[k] != ',') k++;
        k++;
        //到达左子树
        return dfs() && dfs();
    }
};