class Solution {
public:
    int maxProduct(vector<string>& words) {
        vector<int> state;
        for(auto word : words) {
            int s = 0;
            for(auto c : word)
                s |= 1 << (c - 'a');
            state.push_back(s);
        }
        int res = 0;
        for(int i = 0; i < words.size(); i++)
            for(int j = i + 1; j < words.size(); j++)
                if((state[i] & state[j]) == 0)
                    res = max(res, (int)(words[i].size() * words[j].size()));
        return res;
    }
};

//位运算优化，快速判断2个单词是否有相同字符