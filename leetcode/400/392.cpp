class Solution {
public:
    bool isSubsequence(string s, string t) {
        int k = 0;
        for(auto c : t) {
            if(k < s.size() && s[k] == c) k++;
        }       
        return k == s.size();
    }
};
//双指针