class RandomizedCollection {
public:
    unordered_map<int, unordered_set<int>> hash;
    vector<int> nums;

    /** Initialize your data structure here. */
    RandomizedCollection() {

    }

    /** Inserts a value to the collection. Returns true if the collection did not already contain the specified element. */
    bool insert(int x) {
        bool res = hash[x].empty();
        nums.push_back(x);
        hash[x].insert(nums.size() - 1);
        return res;
    }

    /** Removes a value from the collection. Returns true if the collection contained the specified element. */
    bool remove(int x) {
        if (hash[x].size()) {
            int px = *hash[x].begin(), py = nums.size() - 1;
            int y = nums.back();
            swap(nums[px], nums[py]);
            hash[x].erase(px), hash[x].insert(py);
            hash[y].erase(py), hash[y].insert(px);
            nums.pop_back();
            hash[x].erase(py);
            return true;
        }
        return false;
    }

    /** Get a random element from the collection. */
    int getRandom() {
        return nums[rand() % nums.size()];
    }
};

//哈希表套哈希表 + 数组