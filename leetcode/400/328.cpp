/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* oddEvenList(ListNode* head) {
        if(!head || !head->next) return head;

        auto odd = head, even = head->next;
        auto odd2 = head, even2 = even;

        for(auto p = head->next->next; p;) {
            odd2 = odd2->next = p;
            p = p->next;
            if(p) {
                even2 = even2->next = p;
                p = p->next;
            }
        }

        odd2->next = even;
        even2->next = nullptr;
        return odd;
    }
};