class Solution {
public:
    bool isPowerOfThree(int n) {
        return n > 0 && 1162261467 % n == 0;
    }
};


//int 范围最大的就是 3^19
//判断是否n能整除 3^19 即可