class Solution {
public:
    int kthSmallest(vector<vector<int>>& matrix, int k) {
        int l = INT_MIN, r = INT_MAX; 
        while(l < r) {
            int mid = 1ll * l + r >> 1;
            int i = matrix[0].size() - 1, cnt = 0;
            for(int j = 0; j < matrix.size(); j++) {
                while(i >= 0 && matrix[i][j] > mid) i--;
                cnt += i + 1;
            }
            if(cnt >= k) r = mid;
            else l = mid + 1;
        }
        return r;
    }
};

//二分 O(mlog max(martix[i][j]))
//m = matrix[0].size()