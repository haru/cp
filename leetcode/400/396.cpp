class Solution {
public:
    int maxRotateFunction(vector<int>& nums) {
        using ll = long long;
        ll sum = 0, cur = 0;
        for(auto c : nums) sum += c;       
        int n = nums.size();
        for(int i = 0; i <n; i++) cur += i * nums[i];
        ll res = cur;
        for(int i = n - 1; i >= 0; i--) {
            cur += sum - (ll)n * nums[i];
            res = max(res, cur);
        }
        return res;
    }
};