class Solution {
public:
    void wiggleSort(vector<int>& nums) {
        int n =nums.size();
        auto midptr = nums.begin() + n / 2;
        nth_element(nums.begin(), midptr, nums.end()); 

        int mid = *midptr;

        #define A(i) nums[(1 + 2 * i) % (n | 1)]

        //逆序排序
        for(int i = 0, j = 0, k = n - 1; i <= k;) {
            if(A(i) > mid) swap(A(i++), A(j++));
            else if(A(i) < mid) swap(A(i), A(k--));
            else i++;
        }
    }
};

//O(logn) time, O(logn) space
//依靠3数排序+快速选择求中位数，构造
//https://www.acwing.com/solution/content/350/