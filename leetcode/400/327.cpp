class Solution {
public:
    using ll = long long;
    vector<int> tr;
    vector<ll> numbers;
    int m;

    ll get(ll x) {
        return lower_bound(numbers.begin(), numbers.end(), x) - numbers.begin() + 1; //下标从1开始
    }

    int lowbit(int x) {
        return x & -x;
    }

    void add(int x, int v) {
        for(int i = x; i <= m; i+=lowbit(i)) tr[i] += v;
    }

    int query(int x) {
        int res = 0;
        for(int i = x; i; i-=lowbit(i)) res += tr[i];
        return res;
    }

    int countRangeSum(vector<int>& nums, int lower, int upper) {
        int n = nums.size();
        vector<ll> s(n + 1);
        numbers.push_back(0);
        for(int i = 1; i <= n; i++) {
            s[i] = s[i-1] + nums[i-1];
            numbers.push_back(s[i]);
            numbers.push_back(s[i] - lower);
            numbers.push_back(s[i] - upper - 1);
        }
        sort(numbers.begin(), numbers.end());
        numbers.erase(unique(numbers.begin(), numbers.end()), numbers.end());
        m = numbers.size();
        tr.resize(m + 1);
        int res = 0;
        add(get(0), 1);
        // s[j] - s[i] 没有考虑前缀和 s[j] 本身这种情况，单独加入一个为0的前缀和，当计算 s[j] - 0 时候，就计算到了 s[j] 本身这个前缀和
        for (int i = 1; i <= n; i ++ ) {
            res += query(get(s[i] - lower)) - query(get(s[i] - upper - 1));
            add(get(s[i]), 1);
        }

        return res;
    }
};

//平衡树 or 离散化+树状数组