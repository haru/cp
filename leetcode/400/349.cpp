class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        unordered_set<int> s;
        for(auto &x : nums1) s.insert(x);
        vector<int> res;
        for(auto &x : nums2) if(s.count(x)) {
            res.push_back(x);
            s.erase(x);
        }
        return res;
    }
};