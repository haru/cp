class Solution {
public:
    bool isPowerOfTwo(int _n) {
        long long n = _n;
        if(n && (n & -n) == n) return true;
        return false; 
    }
};