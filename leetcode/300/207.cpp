class Solution {
public:
    bool canFinish(int n, vector<vector<int>>& edges) {
        vector<vector<int>> g(n);
        vector<int> d(n);
        queue<int> q;
        for(auto& e : edges) {
            int b = e[0], a = e[1];
            g[a].push_back(b);
            d[b]++;
        }
        for(int i = 0; i < n; i++) if(d[i] == 0) q.push(i);

        int cnt =0;
        while(q.size()) {
            auto t = q.front();
            q.pop();
            cnt++;
            for(auto s : g[t]) if(--d[s] == 0) q.push(s);
        }

        return cnt == n;
    }
};

//拓扑排序模板题