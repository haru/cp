class Solution {
public:
    bool isIsomorphic(string s, string t) {
        if(s.size() != t.size()) return false;

        unordered_map<char, char> hash;
        unordered_set<char> rev;
        for(int i = 0; i < t.size(); i++) {
            if(!hash.count(s[i])) {
                if(!rev.count(t[i])) hash[s[i]] = t[i], rev.insert(t[i]);
                else return false;
            } else {
                if(hash[s[i]] != t[i]) return false;
            }
        }
        return true;
    }
};