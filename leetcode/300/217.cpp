class Solution {
public:
    bool containsDuplicate(vector<int>& nums) {
        unordered_set<int> hash;
        for(auto t :nums) {
            if(hash.count(t)) return true;
            else hash.insert(t);
        }
        return false;
    }
};