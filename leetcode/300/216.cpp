class Solution {
public:
    vector<vector<int>> ans;
    vector<int> path;
    vector<vector<int>> combinationSum3(int k, int n) {
        dfs(1, n, k);
        return ans;      
    }

    void dfs(int start, int n, int k) {
        if(!n && !k) ans.push_back(path);
        else {
            if(k){
                for(int i = start; i <= 9; i++)
                    if(n >= i) {
                        path.push_back(i);
                        dfs(i + 1, n - i, k-1);
                        path.pop_back();
                    }
            }
        } 
    }
};

//DFS 搜组合方案, 用 start 标记顺序，每次只搜后面的数
//复杂度 C(9, k)