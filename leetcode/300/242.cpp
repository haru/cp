class Solution {
public:
    bool isAnagram(string s, string t) {
        unordered_map<char, int> a,b;
        for(auto c : s) a[c]++;
        for(auto c : t) b[c]++;
        return a == b;
    }
};