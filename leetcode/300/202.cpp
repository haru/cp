class Solution {
public:
    int get(int n) {
        int res = 0;
        while(n) {
            res += (n % 10) * (n%10);
            n /= 10;
        }
        return res;
    }
    bool isHappy(int n) {
        int fast = get(n), slow = n;
        while(fast != slow) {
            fast = get(get(fast));
            slow = get(slow);
        } 

        return fast == 1;
    }
};
//最大不超过 999.9999, 10个9，也就是10*81 = 810
//所以811次必然出现重复的结果，也就是进入到一个环中
//问题就转换为看环中是否有1，其实环中全部都是1