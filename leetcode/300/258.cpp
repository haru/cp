class Solution {
public:
    int addDigits(int num) {
        if(!num) return 0;
        if(num % 9) return num % 9;
        return 9;
    }
};

//12345 和 1 + 2 + 3 +... + 5 两者 mod(9) 的余数相同
//f(x) = x mod 9
//这样一直迭代，无论多少次，mod 9 的余数不会变