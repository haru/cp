class Solution {
public:
    int hIndex(vector<int>& c) {
        int n = c.size();
        int l = 0, r = n; // r = n -1 不能处理 [1] 的情况
        while(l < r) {
            int mid = l + r + 1 >> 1;
            if(c[n-mid] >= mid) l = mid;
            else r = mid - 1;
        }
        return r;
    }
};

//二分