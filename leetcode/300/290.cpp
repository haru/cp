class Solution {
public:
    bool wordPattern(string pattern, string str) {
        vector<string> words;
        stringstream ss(str);
        string word;
        while(ss >> word) words.push_back(word);
        if(pattern.size() != words.size()) return false;
        unordered_map<char, string> hash;
        unordered_map<string, char> re;
        for(int i = 0; i < pattern.size(); i++) {
            if(!hash.count(pattern[i]) && !re.count(words[i])) {
                hash[pattern[i]] = words[i];
                re[words[i]] = pattern[i];
            } else {
                if(hash.count(pattern[i]) && words[i] != hash[pattern[i]]) return false;
                if(re.count(words[i]) && pattern[i] != re[words[i]]) return false;
            }
        }
        return true;
    }
};