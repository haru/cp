class Solution {
public:
    int findDuplicate(vector<int>& nums) {
        int a = 0, b = 0;
        while(true) {
            a = nums[a];
            b = nums[nums[b]];
            if(a == b) {
                a = 0;
                while(a != b) {
                    a = nums[a];
                    b = nums[b];
                }
                return a;
            }
        }
        return -1;
    }
};

//转换为 P142 找环的入口
//如果没有O(1)空间限制，就可以只统计一下图中每个点的入度即可