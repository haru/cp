class Solution {
public:
    vector<int> majorityElement(vector<int>& nums) {
         int r1, r2, c1 = 0, c2 = 0;
         for(auto &x : nums) {
            if(c1 && x == r1) c1++;
            else if(c2 && x == r2) c2++;
            else if(!c1) r1 = x, c1 ++;
            else if(!c2) r2 = x, c2++;
            else c1 --, c2 --;
         }

         vector<int> res;
         int n = nums.size();
         c1 = 0, c2 = 0;
         for(auto x : nums) {
            if(x == r1) c1++;
            else if(x == r2) c2++; //这里 r1 == r2 注意不能重复计算, 放入任意一个中即可
         }
         if(c1 > n / 3) res.push_back(r1);
         if(c2 > n / 3) res.push_back(r2);
         return res;
    }
};

//摩尔投票算法可以扩展为 (n/k) 次出现的一般情况, 时间复杂度 O(kn)