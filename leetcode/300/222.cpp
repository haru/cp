/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // vector<TreeNode*> st;
    int cnt =0;
    int countNodes(TreeNode* root) {
        dfs(root);
        return cnt;
    }

    void dfs(TreeNode* root) {
        if(!root) return;
        // st[root] = true;
        cnt++;
        if(root->left) dfs(root->left);
        if(root->right) dfs(root->right);
    }
};

//O(n) 解法，但是题目要求必须复杂度严格小于 O(n)
//只能用二分了。。。