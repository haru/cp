class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
        int n = nums.size();
        vector<int> q;
        for(auto x : nums) {
            if(q.empty() || x > q.back()) q.push_back(x);
            else {
                if(x <= q[0]) q[0] = x;
                else {
                    int l = 0, r = q.size() - 1;
                    while(l < r) {
                        int mid = l + r + 1 >> 1;
                        if(q[mid] < x) l = mid;
                        else r = mid - 1;
                    }
                    q[r+1] = x;
                }
            }
        } 
        return q.size();
    }
};

//LIS O(nlogn) 贪心解法
//q[k] = x 意思就是长度为k的上升子序列，结尾元素最小值是 x
//结尾元素越小，后面来的元素就更可能接在后面，序列就更可能越长