class Solution {
public:
    int rob(vector<int>& nums) {
        int n = nums.size();
        if(!n) return 0;
        if(n == 1) return nums[0];

        vector<int> f(n + 1), g(n + 1);
        for(int i = 2; i <= n; i++) {
            f[i] = g[i-1] + nums[i-1];
            g[i] = max(g[i-1], f[i-1]);
        }
        int res = max(f[n], g[n]);
        f[1] = nums[0];
        g[1] = 0;
        for(int i = 2; i <= n; i++) {
            f[i] = g[i-1] + nums[i-1];
            g[i] = max(g[i-1], f[i-1]);
        }

        return max(res, g[n]);
    }
};

//从任何一点断开环，变成链即可，比如1号点
//然后分为选1不能选n，选n不能选1讨论 DP