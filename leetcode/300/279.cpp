class Solution {
public:
    bool check(int x) {
        int r = sqrt(x);
        if(r * r == x) return true;
        return false;
    }
    int numSquares(int n) {
        if(check(n)) return 1;
        for(int a = 1; a <= n / a; a++) {
            if(check(n - a * a)) return 2;
        } 
        while(n % 4 == 0) n/= 4;
        if(n % 8 != 7) return 3;
        return 4;
    }
};


//https://www.acwing.com/solution/content/300/
//O(sqrt(n)+logn)  拉格朗日四平方和定理 +  勒让德三平方和定理