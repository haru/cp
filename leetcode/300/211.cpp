class WordDictionary {
public:
    struct Node {
        bool is_end;
        Node* son[26];
        Node() {
            is_end = false;
            for(int i = 0; i < 26; i++) son[i] = nullptr;
        }
    }*root;
    WordDictionary() {
        root = new Node(); 
    }
    
    void addWord(string word) {
       auto p = root;
       for(auto c : word) {
            int u = c - 'a';
            if(!p->son[u]) p->son[u] = new Node();
            p = p->son[u];
       } 
       p->is_end = true;
    }
    
    bool search(string word) {
       return dfs(root, word, 0); 
    }

    bool dfs(Node* p, string s, int u) {
        if(u == s.size()) return p->is_end;

        if(s[u] != '.')  {
            int i = s[u] - 'a';
            if(!p->son[i]) return false;
            return dfs(p->son[i], s, u + 1);
        } else {
            for(int i = 0; i < 26; i++) {
                if(p->son[i] && dfs(p->son[i], s, u + 1)) return true;
            }
            return false;
        }
        // return true;
        return false;
    }
};

/**
 * Your WordDictionary object will be instantiated and called as such:
 * WordDictionary* obj = new WordDictionary();
 * obj->addWord(word);
 * bool param_2 = obj->search(word);
 */

//Trie + DFS 遇到 '.' 就只能暴搜了