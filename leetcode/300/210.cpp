class Solution {
public:
    vector<int> findOrder(int n, vector<vector<int>>& edges) {
        vector<vector<int>> g(n);
        vector<int> res;
        vector<int> d(n);

        for(auto t : edges) {
            int b = t[0], a = t[1];
            g[a].push_back(b);
            d[b] ++;
        } 
        queue<int> q;
        for(int i = 0; i < n; i++) if(d[i] == 0) q.push(i);
        while(q.size()) {
            auto t = q.front();
            q.pop();
            res.push_back(t);
            for(auto s : g[t]) if(--d[s] == 0) q.push(s);
        }
        if(res.size() == n) return res;
        else return {};
    }
};