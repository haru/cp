class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        int k = 0;
        for(auto x : nums) if(x) nums[k++] = x;
        for(int i = k; i < nums.size(); i++) nums[i] = 0;
    }
};