class Solution {
public:
    bool containsNearbyAlmostDuplicate(vector<int>& nums, int k, int t) {
        multiset<long long> S;
        S.insert(1e18);
        S.insert(-1e18);

        for(int i = 0, j = 0; i < nums.size(); i++) {
            int x = nums[i];
            if(i - j > k) S.erase(S.find(nums[j++]));
            auto it = S.lower_bound(nums[i]);
            if(*it - x <= t) return true;
            it --;
            if(x - *it <= t) return true;
            S.insert(x);
        }
        return false;
    }
};

//滑动窗口 O(nlogn)