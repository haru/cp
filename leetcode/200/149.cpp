class Solution {
public:
    int maxPoints(vector<vector<int>>& points) {
        int res = 0;
        for(auto p : points){
            int ss = 0, vs = 0;
            unordered_map<long double, int> cnt;
            for(auto q : points) {
                if(p == q) ss++;
                else if(p[0] == q[0]) vs++;
                else {
                    long double k = (long double)(q[1] - p[1]) / (q[0] - p[0]);
                    cnt[k]++;
                }
                int c = vs;
                for(auto [k, t] : cnt) c = max(c, t);
                res = max(res, c + ss);
            }
        }
        return res;
    }
};

//每2点求斜率，然后以斜率为key建哈希表
//每次枚举都开新哈希表，但是这样其实多了很多无用计算，比如 x, y 枚举y的时候会重新算一次这条直线的所有点...