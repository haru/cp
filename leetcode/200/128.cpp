class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
        unordered_set<int> S;
        for(auto & t : nums) S.insert(t);

        int res = 0;
        for(auto &x : nums) {
            if(S.count(x) && !S.count(x-1)) {
                S.erase(x);
                int y = x;
                while(S.count(y + 1)) y++, S.erase(y);
                res = max(res, y - x + 1);
            }
        }
        return res;
    }
};