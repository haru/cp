class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int two = 0, one = 0;
        for(auto x : nums) {
            one = (one ^ x) & ~two;
            two = (two ^ x) & ~one;
        }
        return one;
    }
};

//位元算，画真值表，状态机