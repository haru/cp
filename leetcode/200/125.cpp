class Solution {
public:
    bool isPalindrome(string s) {
        string temp;
        for(auto t : s){
            if(t >= 'a' && t <= 'z' || t >='A' && t <='Z' || t >= '0' && t <= '9') temp += tolower(t);
        }       
        string t2 = temp;
        reverse(temp.begin(), temp.end());
        if(t2 == temp) return true;
        return false;
    }
};