class Solution {
public:
    vector<string> findRepeatedDnaSequences(string s) {
        unordered_map<string, int> cnt;
        for(int i = 0; i + 10 <= s.size(); i++) {
            cnt[s.substr(i, 10)]++;
        }
        vector<string> res;
        for(auto [s, c] : cnt) if(c > 1) res.push_back(s);
        return res;
    }
};