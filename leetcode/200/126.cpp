class Solution {
public:
    unordered_set<string> S;
    unordered_map<string, int> dist;
    queue<string> q;
    vector<vector<string>> ans;
    vector<string> path;
    string beginWord;

    vector<vector<string>> findLadders(string _beginWord, string endWord, vector<string>& wordList) {
        for(auto t : wordList) S.insert(t);
        beginWord = _beginWord;

        dist[beginWord] = 0;
        q.push(beginWord);
        while(q.size()) {
            auto t = q.front();
            q.pop();

            string r = t;
            for(int i = 0; i < t.size(); i++) {
                t = r;
                for(char j = 'a'; j <='z'; j++){
                    t[i] = j;
                    if(S.count(t) && dist.count(t) == 0) { // !dist.count(t)
                        dist[t] = dist[r] + 1;
                        if(t == endWord) break;
                        q.push(t);
                    }
                }
            }
        } 

        if(dist.count(endWord) == 0) return ans;
        path.push_back(endWord);
        dfs(endWord);
        return ans;
    }

    void dfs(string t) {
        if(t == beginWord) {
            reverse(path.begin(), path.end());
            ans.push_back(path);
            reverse(path.begin(), path.end());
        } else {
            string r = t;
            for(int i = 0; i < t.size(); i++){
                t = r;
                for(char j = 'a'; j <= 'z'; j++){
                    t[i] = j; 
                    // beginWord 可能不在 S 里面, 所以是 dist.count(t)
                    // 但是 endWord 一定在 wordList 里面
                    if(dist.count(t) && dist[t] + 1 == dist[r]) {
                        path.push_back(t);
                        dfs(t);
                        path.pop_back();
                    }
                }
            }
        }
    }
};
//为什么需要从 endWord 反向DFS得到所有路径搜索更快: https://www.acwing.com/activity/content/code/content/394559/

