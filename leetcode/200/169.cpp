class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int r,  c = 0;
        for(auto x : nums) {
            if(c == 0) {
                r = x;
                c = 1;
            }else {
                if(x != r) c--;
                else c++;
            }
        }
        return r;
    }
};

//Time: O(n), Space: O(1)