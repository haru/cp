/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    unordered_set<ListNode*> hash;
    bool hasCycle(ListNode *head) {
        for(auto p = head; p; p = p->next) {
            if(!hash.count(p)) hash.insert(p);
            else return true;
        } 
        return false;
    }
};