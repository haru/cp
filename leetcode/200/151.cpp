class Solution {
public:
    string reverseWords(string s) {
        vector<string> all;
        string res, temp = "";
        for(auto t : s) {
            if(t != ' ') temp += t;
            else {
                if(temp.size()) all.push_back(temp);
                temp = "";
            }
        }
        if(temp.size()) all.push_back(temp);
        reverse(all.begin(), all.end());
        for(auto t : all) {
            res  = res + t + " ";
        }
        res.pop_back();
        return res;
    }
};