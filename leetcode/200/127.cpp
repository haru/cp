class Solution {
public:
    int ladderLength(string beginWord, string endWord, vector<string>& wordList) {
        unordered_set<string> S;
        for(auto t : wordList) S.insert(t);
        unordered_map<string, int> dist;
        dist[beginWord] = 0;
        queue<string> q;
        q.push(beginWord);

        while(q.size()) {
            auto t = q.front();
            q.pop();
            string r = t;
            for(int i = 0; i < t.size(); i++) {
                t = r;
                for(char j = 'a'; j <= 'z'; j++){
                    t[i] = j;
                    if(S.count(t) && !dist.count(t)) { //这里不能写 dist[t] == 0 因为beginWord也在list的话 就会重复计算+1
                        dist[t] = dist[r] + 1;
                        if(t == endWord) return dist[t] + 1; // 起点也要算上，题意要求
                        q.push(t);
                    }
                }
            }
        }
        return 0;
    }
};