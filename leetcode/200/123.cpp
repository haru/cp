class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int n = prices.size();
        vector<int> f(n + 2);

        int minp = prices[0];
        for(int i = 1; i < n; i++){
            f[i] = max(f[i-1], prices[i] - minp);
            minp = min(minp, prices[i]);
        }
        int maxp = prices[n-1];
        long long res = 0;
        for(int i = n - 2; i >= 0; i --) {
            res = max(res, 1ll*f[i] + maxp - prices[i]);
            maxp = max(maxp, prices[i]);
        }

        return res;
    }
};

//前后缀分解