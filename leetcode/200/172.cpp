class Solution {
public:
    int trailingZeroes(int n) {
        int cnt1 = 0, cnt2 = 0;
        for(int i = 1; i <= n; i++) {
            int t1 = i, t2 = i;
            while(t1 % 5 == 0) t1 /= 5, cnt1++;
            while(t2 % 2 == 0) t2 /= 2, cnt2++;
        }
        return min(cnt1, cnt2);
    }
};