class Solution {
public:
    bool wordBreak(string s, vector<string>& wordDict) {
        using ull = unsigned long long;
        const int P = 131;
        unordered_set<ull> hash;
        for(auto & word : wordDict) {
            ull h = 0;
            for(auto c : word) h = h * P + c;
            hash.insert(h);
        }

        int n = s.size();
        vector<bool> f(n + 1);
        s = ' ' + s;
        f[0] = true;

        for(int i = 0; i < n; i++)
            if(f[i]) {
                ull h = 0;
                for(int j = i + 1; j <= n; j++) { //正反枚举貌似复杂度有区别, 按照 P132 的枚举方法
                    h = h * P + s[j];
                    if(hash.count(h)) f[j] = true;
                }
            } 
        return f[n];
    }
};

//DP O(n^2)