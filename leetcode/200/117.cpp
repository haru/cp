/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() : val(0), left(NULL), right(NULL), next(NULL) {}

    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    Node(int _val, Node* _left, Node* _right, Node* _next)
        : val(_val), left(_left), right(_right), next(_next) {}
};
*/

class Solution {
public:
    Node* connect(Node* root) {
        if(!root) return root;
        auto cur = root; 
        while(cur) {
            auto head = new Node(-1);
            auto tail = head;
            for(auto p = cur; p; p = p->next) {
                if(p->left) tail = tail->next = p->left;
                if(p->right) tail = tail->next = p->right;
            }    
            cur = head->next;
        }
        return root;
    }
};

//相比 p116 就是改成了非完全二叉树
//bilibili.com/video/BV1uU4y1U7xN/ 这个视频讲的很好思路