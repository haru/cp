/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *detectCycle(ListNode *head) {
        if(!head || !head->next) return nullptr;

        auto s = head, f = head;
        while(f) {
            s = s->next, f = f->next; //注意两个开始在一起
            if(!f) return nullptr;
            f = f->next;

            if(s == f) {
                s = head;
                while(s!=f) s = s->next, f = f->next;
                return s;
            }
        }
        return nullptr;
    }
};

//快慢指针，推公式