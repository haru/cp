class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        for(int i = 0, j = nums.size() - 1; i < j; i++) {
            while(i < j && nums[i] + nums[j] > target) j--;
            if(i < j && nums[i] + nums[j] == target) return {i + 1, j + 1};
        }
        return {};
    }
};