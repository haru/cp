/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* sortedListToBST(ListNode* head) {
        if(!head) return nullptr;

        vector<int> nums;
        for(auto p = head; p; p = p->next) nums.push_back(p->val);       
        return build(nums, 0, nums.size()-1);
    }

    TreeNode* build(vector<int>& nums, int l, int r){
        if(l > r) return nullptr;
        int mid = l + r >> 1;
        auto root = new TreeNode(nums[mid]);
        root->left = build(nums, l, mid-1);
        root->right = build(nums, mid + 1, r);
        return root;
    }

};

//用额外空间就是开个数组，和 p108 一样
//不用额外空间就是每次都手动找中点 复杂度O(nlogn)