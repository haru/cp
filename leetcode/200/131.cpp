class Solution {
public:
    vector<vector<string>> ans;
    vector<string> path;
    vector<vector<bool>> f;
    int n;
    vector<vector<string>> partition(string s) {
        n = s.size();
        f = vector<vector<bool>>(n, vector<bool>(n));
        for(int j = 0; j < n; j++)  //注意枚举顺序 i 用到 i + 1 的状态所以先枚举 j
            for(int i = 0; i <= j; i++) {
                if(i == j) f[i][j] = true;
                else if(s[i] == s[j]){
                    if(j && (i + 1 > j - 1 || f[i + 1][j-1])) f[i][j] = true; // i + 1 > j - 1 只有两个字符
                }
            }
        dfs(s, 0);
        return ans;
    }

    void dfs(string& s, int u) {
        if(u == s.size()) {
            ans.push_back(path);
            return;
        }

        for(int i = u; i < s.size(); i++) {
            if(f[u][i]) {
                path.push_back(s.substr(u, i -u + 1)); //substring 会有重复的，不需要去重
                dfs(s, i + 1);
                path.pop_back();
            }
        } 
    }
};

//O(2^n) 暴搜
//f[i][j]: s[i~j] 是回文串