class Solution {
public:
    int minimumTotal(vector<vector<int>>& f) {
        for(int i = f.size() - 2; i >= 0; i--)
            for(int j = 0; j <= i; j++) // 注意 j 取值范围
                f[i][j] += min(f[i+1][j], f[i+1][j+1]);
    return f[0][0];
    }
};
//从下往上算，不用考虑边界情况