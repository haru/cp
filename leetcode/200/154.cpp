class Solution {
public:
    int findMin(vector<int>& nums) {
        if(nums.size() == 1) return nums[0];
        int l = 0, r = nums.size() - 1;
        while(l < r && nums[r] == nums[0]) r --;

        if(l >= r) return nums[0];

        while(l < r) {
            int mid = l + r + 1 >> 1;
            if(nums[mid] >= nums[0]) l = mid;
            else r = mid - 1;
        }

        if(l == nums.size() - 1) return nums[0];
        else return nums[l + 1];
    }
};