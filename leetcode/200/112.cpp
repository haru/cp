/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    unordered_set<int> res;
    bool hasPathSum(TreeNode* root, int targetSum) {
        if(!root) return false;
        dfs(root, 0);
        if(res.count(targetSum)) return true;
        else return false;
    }
    void dfs(TreeNode* root, int sum) {
        if(!root ->left && !root->right) res.insert(sum + root->val);
        if(root->left) dfs(root->left, sum + root->val);
        if(root->right) dfs(root->right, sum + root->val);
    }
};