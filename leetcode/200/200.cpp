class Solution {
public:
    int dx[4] = {-1, 0, 1, 0}, dy[4] = {0, 1, 0, -1};
    vector<vector<char>> grid;
    int numIslands(vector<vector<char>>& _grid) {
        grid = _grid;
        int cnt = 0;
        for(int i = 0; i < grid.size(); i++)
            for(int j = 0; j < grid[0].size(); j++){
                if(grid[i][j] == '1'){
                    dfs(i, j);
                    cnt++;
                }
            }
        return cnt;
    }

    void dfs(int x, int y) {
        grid[x][y] = '0';
        for(int i = 0; i < 4; i++) {
            int a = x + dx[i], b = y + dy[i];
            if(a < 0 || a >= grid.size() || b < 0 || b >= grid[0].size()) continue;
            if(grid[a][b] == '1') dfs(a, b);
        }
    }
};