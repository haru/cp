class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        int n = nums.size();
        k %= nums.size();
        vector<int> res;
        for(int i = n - k; i < n; i++) res.push_back(nums[i]);
        for(int i = 0; i < n - k; i++) res.push_back(nums[i]);
        nums = res;
    }
};

//Space O(n) solution