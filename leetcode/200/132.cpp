class Solution {
public:
    int minCut(string s) {
        int n = s.size(); 
        s = ' ' + s;
        vector<vector<bool>> g(n + 1, vector<bool>(n+1));

        for(int j = 1; j <= n; j++)
            for(int i = 1; i <= j; i++) {
                if(i == j) g[i][j] = true;
                else if(s[i] == s[j]) {
                    if(i + 1 > j - 1 || g[i+1][j-1]) g[i][j] = true;
                }
            }

        vector<int> f(n + 1, 0x3f3f3f3f);
        f[0] = 0;
        for(int i = 1; i<= n; i++)
            for(int j = 1; j <= i; j++)
                if(g[j][i])  //注意下标顺序 回文串
                    f[i] = min(f[i], f[j-1] + 1);

        return f[n] - 1; // 答案就是隔板的数量
    }
};

//f[i]: 分割 s[1~i] 的最小段数
// DP O(n^2)