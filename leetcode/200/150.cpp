class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        stack<string> stk;
        unordered_set<string> symbols;
        symbols.insert("+");
        symbols.insert("-");
        symbols.insert("*");
        symbols.insert("/");
        for(auto t : tokens) {
            if(!symbols.count(t)) stk.push(t);
            else {
                auto t1 = stk.top();stk.pop();
                auto t2 = stk.top();stk.pop();
                int res = 0;
                if(t == "+") res = stoi(t1) + stoi(t2), stk.push(to_string(res)); 
                if(t == "-") res = stoi(t2) - stoi(t1), stk.push(to_string(res)); 
                if(t == "*") res = stoi(t1) * stoi(t2), stk.push(to_string(res)); 
                if(t == "/") res = stoi(t2) / stoi(t1), stk.push(to_string(res)); 
            }
        }
        return stoi(stk.top());
    }
};