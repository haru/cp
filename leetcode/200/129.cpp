/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    long long ans = 0;
    string path;
    int sumNumbers(TreeNode* root) {
        dfs(root);
        return ans; 
    }
    void dfs(TreeNode *u) {
        if(!u->left && !u->right) {
            path += to_string(u->val);
            cout << path << endl;
            ans+=stoll(path);
            return;
        }
        path += to_string(u->val);
        int cnt = 0;
        if(u->left){
            // path += to_string(u->left->val);
            dfs(u->left);
            path.pop_back();
        }

        if(u->right){
            // path += to_string(u->right->val);
            dfs(u->right);
            path.pop_back();
        }
    }
};