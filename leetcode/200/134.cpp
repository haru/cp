class Solution {
public:
    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
        int n = gas.size();
        for(int i = 0, j; i < n;) {
            int left = 0;
            for(j = 0; j < n; j++) { // 走几站
                int k = (i + j) % n;
                left += gas[k] - cost[k];

                if(left < 0) break;
            }

            if(j == n) return i;
            i = i + j + 1;
        } 

        return -1;
    }
};

//贪心思路 时间 O(n) 空间 O(1)