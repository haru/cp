class Solution {
public:
    int maxProduct(vector<int>& nums) {
        int res = nums[0];
        int f = nums[0], g = nums[0]; //max, min
        for(int i = 1; i < nums.size(); i++) {
            int a = nums[i], fa = f * a, ga = g * a;
            f = max({a, fa, ga});
            g = min({a, fa, ga});
            res = max(res, f);
        }     
        return res;
    }
};

//DP
//维护当前nums[i]结尾的最大和最小乘积子数组