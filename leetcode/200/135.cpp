class Solution {
public:
    vector<int> w;
    vector<int> f;
    int n;

    int candy(vector<int>& ratings) {
        n = ratings.size();
        w = ratings;
        f.resize(n, -1);

        int res = 0;
        for(int i = 0 ;i < n; i++) res += dp(i);
        return res;      
    }

    int dp(int x){
        if(f[x] != -1)return f[x];
        f[x] = 1;
        if(x && w[x-1] < w[x]) f[x] = max(f[x], dp(x-1) + 1);
        if(x + 1 < n && w[x+1] < w[x]) f[x] = max(f[x], dp(x + 1) + 1);
        return f[x];
    }
};

//记忆化搜索 DP O(N) 记忆化之后每个 f[x] 只会被计算一次