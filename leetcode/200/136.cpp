class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int res = 0;
        for(auto &t : nums) res ^= t;
        return res;
    }
};