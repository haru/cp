class Solution {
public:
    bool isScramble(string s1, string s2) {
        int n = s1.size();
        //f[i][j][k] 第三维长度，会取到下标 n, 需要开到 n + 1
        vector<vector<vector<bool>>> f(n, vector<vector<bool>>(n, vector<bool>(n + 1)));    


        for(int k = 1; k <= n; k++)
            for(int i = 0; i + k - 1 < n; i++)
                for(int j = 0; j + k - 1 < n; j++) {
                    if(k == 1) {
                        if(s1[i] == s2[j]) f[i][j][k] = true;
                    }
                    else { //翻转或者不翻转
                        for(int u = 1; u <= k - 1; u++) { // 第一段的长度
                            if(f[i][j][u] && f[i + u][j + u][k - u] || //不翻转
                                f[i][j + k -u][u] && f[i + u][j][k - u]){
                                f[i][j][k] = true;
                                break;
                            }
                        } 
                    }
                }
        return f[0][0][n];
    }
};

//f[i][j][k]: s1 i 开始的，与 s2 j 开始的长度为 k 的一段是否可以变换为相等
//区间DP O(N^4)
//https://www.acwing.com/video/2791/