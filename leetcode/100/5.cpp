class Solution {
public:
    string longestPalindrome(string s) {
        //暴力枚举中点 O(N^2)
        // Manacher O(N)

        string res;
        for(int i = 0; i< s.size(); i++) {
            int l = i - 1, r = i + 1; //odd size
            while(l >=0 && r < s.size() && s[l] == s[r]) l--, r++;
            // r - 1 - (l + 1) + 1
            if(res.size() < r - l -1) res = s.substr(l+1, r - l -1);

            l = i, r = i + 1;
            //even size
            while(l >=0 && r < s.size() && s[l] == s[r]) l--, r++;
            if(res.size() < r - l -1) res = s.substr(l+1, r - l -1);
        }

        return res;
    }
};