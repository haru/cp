class Solution {
public:
    vector<int> grayCode(int n) {
        vector<int> res(1, 0);
        while(n --) {
            for(int i = res.size() - 1; i >= 0; i--){
                res[i] *= 2;
                res.push_back(res[i] + 1);
            }
        } 
        return res;
    }
};

// gray code 生成技巧: k 位的话，就是把 k-1 位上下轴对称，然后上面一半后面补上一位0，下面一半后面补上一个1