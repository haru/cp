class Solution {
public:
    string getPermutation(int n, int k) {
         string res;
         vector<bool> st(n);

         for(int i = 0; i < n; i++) {
            int f = 1;
            for(int j = 1; j <= n - 1 - i; j++) f *= j; // 0 ~ n-1 位
            for(int j = 0; j < n; j++){
                if(!st[j]) {
                    if(k <= f){
                        res += to_string(j + 1);
                        st[j] = true;
                        break;
                    }
                    k -= f;
                }
            }
         }

         return res;
    }
};

//题目 n 最大只有 9, 9! = 362880
//但是这题卡时间，即使 DFS 也会超时...
//std::next_permutaion() 能过


//枚举每一位计数 O(N^2)
//https://www.acwing.com/solution/content/145/