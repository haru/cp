class Solution {
public:
    int minDistance(string a, string b) {
        int n = a.size(), m = b.size();
        a = ' ' + a, b = ' ' + b;

        vector<vector<int>> f(n+1, vector<int>(m + 1, 0x3f3f3f3f));

        for(int i = 0; i <= n; i++) f[i][0] = i;
        for(int j = 0; j <= m; j++) f[0][j] = j;


        for(int i = 1; i <= n; i++)
            for(int j = 1; j <= m; j++){
                f[i][j] = min(f[i-1][j], f[i][j-1]) + 1; //增删 A
                f[i][j] = min(f[i][j], f[i-1][j-1] + (a[i] != b[j])); //替换
            }
        return f[n][m];
    }
};

// O(n^2) DP
// f[i][j]: A[1~i] 修改为 B[1~j] 需要的最小次数