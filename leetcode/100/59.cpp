class Solution {
public:
    vector<vector<int>> generateMatrix(int n) {
        // vector<vector<int>>& matrix
        vector<vector<int>> res(n, vector<int>(n));
        int m = n;

        if(!n) return res;

        int dx[] = {0, 1, 0, -1}, dy[] = {1, 0, -1, 0};

        vector<vector<bool>> st(n, vector<bool>(m));

        for(int i = 0, x = 0, y = 0, d = 0; i < n * m; i++) { // 循环 n * m 次注意
            res[x][y] = i + 1;
            st[x][y] = true;

            int a = x + dx[d], b = y + dy[d];

            if(a < 0 || a >= n || b < 0 || b >= m || st[a][b]) {
                d = (d + 1) % 4;
                a = x + dx[d], b = y + dy[d];
            }

            x = a, y = b;
        }

        return res;
    }
};