class Solution {
public:
    int n, m;
    vector<vector<bool>> st;
    bool exist(vector<vector<char>>& board, string word) {
        n =board.size(), m = board[0].size();
        st = vector<vector<bool>>(n, vector<bool>(m));
        for(int i = 0; i < n; i++)
            for(int j = 0;j < m; j++)
                if(dfs(board, word, 0, i, j)) return true;

        return false;
    }

    bool dfs(vector<vector<char>> & board, string word, int u, int x, int y) {
        if(word[u] != board[x][y]) return false;
        if(u == word.size() - 1) return true; //注意 u
        int dx[] = {-1, 0, 1, 0}, dy[] = {0, 1, 0, -1};

        st[x][y] = true;
        for(int i = 0; i < 4; i++){
            int a = x + dx[i], b = y + dy[i];
            if(a < 0 || a >= n || b < 0 || b >= m || st[a][b]) continue;
            if(dfs(board, word, u + 1, a, b)) return true;
        }
        st[x][y] = false;
        return false;
    }
};