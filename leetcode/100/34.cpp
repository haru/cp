class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        if(nums.empty()) return {-1, -1};

        int l = 0, r = nums.size() - 1;
        while(l < r){
            int mid = l + r >> 1;
            if(target <= nums[mid]) r = mid;
            else l = mid + 1;;
        }      
        int pos1 = r;
        bool flag = true;
        if(nums[r] != target) flag = false;

        l = 0, r = nums.size() - 1;
        while(l < r) {
            int mid = l + r + 1 >> 1;
            if(target >= nums[mid]) l = mid;
            else r = mid - 1;
        }
        int pos2 = r;
        if(nums[r] != target) flag = false;
        if(!flag) return {-1, -1};
        else return {pos1, pos2};
    }
};