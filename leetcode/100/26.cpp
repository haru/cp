class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        int k = 0; 
        for(int i = 0; i < nums.size(); i++) {
            if(!i || nums[i] != nums[i-1])
                nums[k++] = nums[i];
        }
        return k;
    }
};

//双指针 O(N) 
//只有前提是 sorted 才可以这么做 因为下一个元素和上一个不一样，必定都比前面的大，没出现过