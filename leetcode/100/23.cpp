/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    struct cmp {
        bool operator() (ListNode * a, ListNode * b){
            return a->val > b->val;
        }
    };

    ListNode* mergeKLists(vector<ListNode*>& lists) {
        priority_queue<ListNode*, vector<ListNode*>, cmp> heap;
        auto dummy = new ListNode(-1), tail = dummy;

        for(auto t : lists) 
            if(t) heap.push(t); // 数据里面有空链表

        while(heap.size()){
            auto t = heap.top();
            heap.pop();
            tail = tail->next = t;
            if(t -> next) heap.push(t->next);
        }
        return dummy -> next; 
    }
};

//用堆优化一下，不用每次都遍历找最小值
//此题应该也可以 divide & conquer 解决