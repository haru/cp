class Solution {
public:
    vector<vector<int>> ans;
    vector<bool> used;
    vector<int> path;

    vector<vector<int>> permuteUnique(vector<int>& nums) {
        sort(nums.begin(), nums.end()); //排序保证剪枝策略的正确性
        used.resize(nums.size());
        path.resize(nums.size());
        dfs(nums, 0);
        return ans;
    }

    void dfs(vector<int> & nums, int u){
        if(u == nums.size()) {
            ans.push_back(path);
            return;
        }

        for(int i = 0; i < nums.size(); i++)
            if(!used[i]) {
                if(i && nums[i] == nums[i-1] && !used[i-1]) continue; //剪枝 剪去重复的排列
                path[u] = nums[i];
                used[i] = true;

                dfs(nums, u + 1);
                used[i] = false;
            }
    }
};

//枚举每个位置填哪个数 这种枚举方法可以保证得到的排列按照字典序出现