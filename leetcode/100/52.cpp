class Solution {
public:
    vector<bool> col, dg, udg;
    vector<vector<string>> ans;
    vector<string> path;
    int n;
    int cnt;
    int totalNQueens(int _n) {
        n = _n;
        col.resize(n);
        dg.resize(n * 2);
        udg.resize(n * 2);

        path = vector<string>(n, string(n, '.'));

        dfs(0);
        return cnt;
    }

    void dfs(int u) {
        if(u == n) {
            // ans.push_back(path); //最后一行后面一行
            cnt++;
            return;
        }


        for(int i = 0; i < n; i++) { //枚举列
            if(!col[i] && !dg[u + i] && !udg[u - i + n]) {
                col[i] = dg[u + i] = udg[u-i + n] = true;
                path[u][i] = 'Q'; 
                dfs(u + 1);
                path[u][i] = '.';
                col[i] = dg[u + i] = udg[u-i + n] = false;
            }
        }
    }
};

//用截距表示对角线
//每个点的坐标 (x,y) 可以求出其所在的两条对角线：y = x + k, y = -x + k
// => k = x - y, k = x + y
// 其中 x - y 可能是负数，为了在数组下标中标记，全部加上偏移量 n 映射一下
//复杂度 O(N! * N^2) N^2 是每次将方案存进去的复杂度