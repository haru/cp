class Solution {
public:
    string minWindow(string s, string t) {
        unordered_map<char, int> hs, ht;
        for(auto c : t) ht[c]++;

        string res;
        int cnt = 0;

        for(int i = 0, j =0; i < s.size(); i++){
            hs[s[i]] ++;
            if(hs[s[i]] <= ht[s[i]]) cnt++; // 注意这里是 <=, “=” 就是恰好最后一个加进来相同的那个字符
            while(hs[s[j]] > ht[s[j]]) hs[s[j]]--, j++;

            if(cnt == t.size())
                if(res.empty() || i - j + 1 < res.size())
                    res = s.substr(j, i - j + 1);
        }
        return res;
    }
};

//滑动窗口 + 哈希 O(N)
//cnt 一旦满足要求之后，之后的程序就只会控制窗口大小了，始终满足有效字符数量 cnt 合法 