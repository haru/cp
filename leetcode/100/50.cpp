class Solution {
public:
    double myPow(double x, int n) {
        using ll = long long; // abs(INT_MIN) 会爆掉 int

        bool minus = n < 0;

        double res = 1;

        for(ll k = abs(n); k; k >>= 1) {
            if(k & 1) res = res * x;
            x = x * x;
        }
        if(minus) res = 1 / res;

        return res;
    }
};