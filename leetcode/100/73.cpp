class Solution {
public:
    void setZeroes(vector<vector<int>>& matrix) {
        if(matrix.empty() || matrix[0].empty()) return;
        int n = matrix.size(), m = matrix[0].size();


        int r = 1, c = 1;
        for(int i = 0; i < n; i++) if(matrix[i][0] == 0) c = 0;
        for(int i = 0; i < m; i++) if(matrix[0][i] == 0) r = 0;

        for(int i = 1; i < m; i++)
            for(int j = 0; j < n; j++)
                if(matrix[j][i] == 0) matrix[0][i] = 0;

        for(int i = 1; i < n; i++)
            for(int j = 0; j < m; j++)
                if(matrix[i][j] == 0) matrix[i][0] = 0;

        for(int i = 1; i < m; i++) if(matrix[0][i] == 0) for(int j = 0; j < n; j++) matrix[j][i] = 0;
        for(int i = 1; i < n; i++) if(matrix[i][0] == 0) for(int j = 0; j < m; j++) matrix[i][j] = 0;

        if(r == 0) for(int i = 0; i < m; i++) matrix[0][i] = 0;
        if(c == 0) for(int i = 0; i < n; i++) matrix[i][0] = 0;
    }
};

//用第一行 第一列表示每行每列的情况
//然后额外开2个变量表示第一行和第一列的情况