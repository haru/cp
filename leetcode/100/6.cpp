class Solution {
public:
    string convert(string s, int n) {
        //找规律 等差数列

        if(n == 1) return s; // corner case 死循环

        string res;

        for(int i = 0; i < n; i++) {
            if(i == 0 || i == n-1) {
                for(int j = i; j < s.size(); j += n * 2 - 2)
                    res += s[j];
            }else {
                for(int j = i, k = 2 * n - 2 - i; j < s.size() ||
                    k < s.size(); j += 2 *n - 2, k += n * 2 - 2) {
                    if(j < s.size()) res += s[j];
                    if(k < s.size()) res += s[k];
                }
            }
        }
        return res;
    }
};