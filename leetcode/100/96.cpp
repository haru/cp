class Solution {
public:
    int numTrees(int n) {
        vector<int> f(n + 1);
        f[0] = 1; 

        for(int i = 1; i <= n; i++)
            for(int j = 1; j <= i; j++)
                f[i] += f[j-1] * f[i-j];

        return f[n];
    }
};
//95 题目一样，求方案数
//f[i]: 长度为 i 的方案数
//DP O(N^2)
//求卡特兰数也可以