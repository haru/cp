class Solution {
public:
    bool isScramble(string s1, string s2) {
        int n = s1.size();

        if(s1 == s2) return true;
        string bs1 = s1, bs2 = s2;
        sort(bs1.begin(), bs1.end());
        sort(bs2.begin(), bs2.end());
        if(bs1 != bs2) return false;


        for(int i = 1; i <= n - 1; i++) { // i 是s1 第一部分的长度
            if(isScramble(s1.substr(0, i), s2.substr(0, i)) && isScramble(s1.substr(i), s2.substr(i))) return true;
            if(isScramble(s1.substr(0, i), s2.substr(n-i)) && isScramble(s1.substr(i), s2.substr(0, n - i))) return true;
        }   
        return false;
    }
};

//暴搜过不了
//这题需要DP