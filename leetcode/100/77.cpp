class Solution {
public:
    vector<int> path;
    vector<vector<int>> ans;
    int cnt;

    vector<vector<int>> combine(int n, int k) {
        cnt = k;
        dfs(n, 0, 1);
        return ans;
    }

    void dfs(int n, int u, int start){
        if(u == cnt) {
            ans.push_back(path);
            return;
        }

        for(int i = start; i <= n; i++){
            path.push_back(i);
            dfs(n, u + 1, i + 1);
            path.pop_back();
        }
    }
};