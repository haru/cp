class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        int n = matrix.size();

        for(int i = 0; i < n; i++)
            for(int j = 0; j < i; j++)
                swap(matrix[i][j], matrix[j][i]); //对角线反转

        for(int i = 0; i < n; i++)
            for(int j = 0, k = n - 1; j < k; j++, k --) //左右对称反转
                swap(matrix[i][j], matrix[i][k]);
    }
};