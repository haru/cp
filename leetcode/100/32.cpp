class Solution {
public:
    int longestValidParentheses(string s) {
        stack<int> stk;

        int res = 0;
        for(int i = 0, start = -1; i < s.size(); i++) {
            if(s[i] == '(') stk.push(i);
            else {
                if(stk.size()){
                    stk.pop();
                    if(stk.size()) res = max(res, i - stk.top());
                    else res = max(res, i - start);
                }
                else start = i;
            }
        }        
        return res;
    }
};

//O(N) 性质就是合法的括号序列不会横跨两个不合法段段临界点