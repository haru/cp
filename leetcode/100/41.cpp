class Solution {
public:
    //O(N) space
    int firstMissingPositive2(vector<int>& nums) {
        unordered_set<int> hash;
        for(auto x: nums) hash.insert(x);

        int res = 1;
        while(hash.count(res)) res++;

        return res; 
    }

    //O(1) space
    int firstMissingPositive(vector<int> & nums) {
        int n = nums.size();
        if(!n) return 1;
        for(auto & x : nums) {
            if(x != INT_MIN)x --;
        }

        for(int i = 0; i < n; i++) {
            while(nums[i] >= 0 && nums[i] < n && nums[i] != i && nums[i] != nums[nums[i]])
                swap(nums[i], nums[nums[i]]);
        }

        for(int i = 0; i < n; i++)
            if(nums[i] != i) return i + 1;

        return n + 1;
    }
};

