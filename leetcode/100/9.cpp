class Solution {
public:
    bool isPalindrome(int x) {
        if(x < 0) return false;
        string sx = to_string(x);
        string rx = sx;
        reverse(rx.begin(), rx.end());
        if(rx == sx) return true;
        else return false;
    }
};