class Solution {
public:
    bool isMatch(string s, string p) {
        /*
        DP, bool f[i][j]: s[1~i] 和 p[1~j] 是否匹配
        1. p[j] != '*'
        2. p[j] == '*' 
        然后第二种情况可以类比完全背包 DP 的思路，借助上一阶段状态优化
        复杂度 O(N^2)
        */
        int n = s.size(), m = p.size();
        s = ' ' + s, p = ' ' + p; // index start from 1
        vector<vector<bool> > f(n + 1, vector<bool>(m+1));
        f[0][0] = true;
        for(int i = 0; i <= n; i++) // s[i] i 从0开始，因为s为空也可以和'*'匹配的
            for(int j = 1; j <= m; j++) { //但是 p[j] j 是0 空串没有意义
                if(j + 1 <=m && p[j+1] == '*') continue; // "x*" 直接等到处理 * 跳过处理x
                if(i > 0 && p[j] != '*') {
                    f[i][j] = f[i-1][j-1] && (s[i] == p[j] || p[j] == '.');
                } else if(p[j] == '*') {
                    //匹配 0，1，2 个字符
                    f[i][j] = f[i][j-2] || i&&f[i-1][j] && (s[i] == p[j-1] || p[j-1] == '.');
                }
            }
        return f[n][m];
    }
};