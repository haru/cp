class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int res = INT_MIN;
        int last = 0;
        for(int i = 0; i < nums.size(); i++) {
            last = nums[i] + max(last, 0); // f[i] = max{nums[i], f[i-1] + nums[i]}
            res = max(res, last);
        }
        return res;
    }
};


//DP O(N) space: O(1)
//f[i]: nums[i] 为结尾的最大子段和