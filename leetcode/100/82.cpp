/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        auto dummy = new ListNode(-1);
        dummy -> next = head;

        auto p = dummy;
        while(p -> next) {
            auto q = p -> next -> next;
            while(q && q->val == p->next->val) q = q -> next;
            if(p->next->next == q) p = p -> next; // q 没动说明只有一个值
            else p -> next = q;
        }

        return dummy -> next;
    }
};

//双指针
// p x x x x q
// 中间这一段相同
// p -> next = q 即可