class Solution {
public:
    vector<vector<int>> ans;
    vector<int> path;
    vector<vector<int>> subsetsWithDup(vector<int>& nums) {
        sort(nums.begin(), nums.end()); 

        dfs(nums, 0);
        return ans;
    }

    void dfs(vector<int> & nums, int u) {
        if(u == nums.size()){
            ans.push_back(path);
            return;
        }

        int k = u + 1;
        while(k < nums.size() && nums[k] == nums[u]) k++;

        for(int i = 0; i <= k - u; i++) { 
            dfs(nums, k); //22, 23 两行代码不能交换顺序
            path.push_back(nums[u]);
        }

        for(int i = 0; i <= k - u; i++) path.pop_back();
    }
};

//就是暴搜每一个数选 0, 1个，2个，。。。
//可以用哈希表记录每个数的个数，或者排序也可以