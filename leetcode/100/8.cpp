class Solution {
public:
    int myAtoi(string str) {
        // 不使用 long long 的写法
        // 只使用 int + 特判

        int res = 0;
        int k = 0;
        while(k < str.size() && str[k] == ' ') k++;
        if(k == str.size()) return 0;

        // int minus = 1; // sign
        // while(k < str.size() && (str[k] == '+' || str[k] == '-')) {
        //     if(str[k] == '+') k++;
        //     if(str[k] == '-') minus *= -1, k++;
        // }


        int minus = 1;
        if (str[k] == '-') minus = -1, k ++ ;
        if (str[k] == '+')
            if (minus == -1) return 0;
            else k ++ ;

        while(k < str.size() && str[k] >= '0' && str[k] <= '9') {
            int x = str[k] - '0';
            if(minus > 0 && res > (INT_MAX - x) / 10) return INT_MAX;
            if(minus < 0 && -res < (INT_MIN + x) / 10) return INT_MIN;
            // corner case : abs(-2147483648 INT_MIN) > INT_MAX
            // 如果不特判断 23 行会溢出
            if(-res * 10 - x == INT_MIN) return INT_MIN;
            res = res * 10 + x;
            k++;
            if(res > INT_MAX) break;
        }
        res *= minus;

        return res;
    }
};