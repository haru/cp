class Solution {
public:
    int threeSumClosest(vector<int>& nums, int target) {
        sort(nums.begin(), nums.end());
        pair<int, int> res = {INT_MAX, INT_MAX};
        for(int i = 0; i < nums.size(); i++)
            for(int j = i + 1, k = nums.size() - 1; j < k; j++) {
                while(k - 1 > j && nums[i] + nums[j] + nums[k-1] >= target) k--;
                int s = nums[i] + nums[j] + nums[k];
                res = min(res, make_pair(abs(s - target), s)); //abs() 考虑 <target 的情况
                if(k - 1 > j) { // 存在 >= target 的三个数, 前一个就是 < target 的临界k
                    s = nums[i] + nums[j] + nums[k-1];
                    res = min(res, make_pair(target - s, s));
                }
            }
        return res.second;
    }
};

//和 15 题差不多
//临界条件改为 nums[i] + nums[j] + nums[k] >= target 以及 < target 的临界 k
// >= target 的 k 向前一个位置就是 < target 的临界 k
// i, j 都是枚举的，所以可以包括所有情况一定可以得到最终的解

//corner case: 不存在 >= target 的3个数字和 

//复杂度 O(N^2)