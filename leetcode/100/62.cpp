class Solution {
public:
    int uniquePaths(int n, int m) {
        vector<vector<int>> f(n, vector<int>(m));

        f[0][0] = 1;
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++){
                if(i) f[i][j] += f[i-1][j];
                if(j) f[i][j] += f[i][j-1];
            }
        return f[n-1][m-1];
    }
};

//DP
//也可以排列组合 C(n-1 + m -1, n-1)