class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        unordered_map<char, int> cnt; //可能会有大写字母 用 cnt[] 数组不行
        int res = 0;
        for(int i = 0, j = 0; i < s.size(); i++) { // j i
            cnt[s[i]] ++;
            while(cnt[s[i]] > 1) {
                cnt[s[j]] --;
                j++;
            }
            res = max(res, i - j + 1);
        }
        return res;
    }
};