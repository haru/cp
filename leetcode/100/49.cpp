class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        map<string, vector<string>> m;

        for(auto s : strs) {
            auto t = s;
            sort(s.begin(), s.end());
            m[s].push_back(t);
        }
        vector<vector<string>> ans;

        for(auto t : m){
            auto arr = t.second;
            vector<string> temp;
            for(auto item : arr) temp.push_back(item);
            ans.push_back(temp);
        }

        return ans;
    }
};