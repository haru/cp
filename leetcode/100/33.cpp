class Solution {
public:
    int search(vector<int>& nums, int target) {
        if(nums.empty()) return -1;

        int l = 0, r = nums.size() - 1;
        while(l < r) {
            int mid = l + r + 1 >> 1;
            if(nums[mid] >= nums[0]) l = mid;
            else r = mid - 1;
        } 
        if(target >= nums[0]) l = 0;
        else l = r + 1, r = nums.size() - 1;

        while(l < r) {
            int mid = l + r + 1 >> 1;
            if(target >= nums[mid]) l = mid;
            else r = mid - 1;
        }

        if(nums[r] == target) return r;
        return -1;
    }
};

//先二分出分段点，然后判断 target 处于哪一段，然后继续二分一次就行了