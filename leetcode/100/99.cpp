/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    void recoverTree(TreeNode* root) {
        TreeNode* first = nullptr, *second, *last = nullptr;
        while(root) {
            if(!root->left) { //没有左子节点直接遍历当前点的右子树
                if(last && last->val > root->val){ //出现逆序对
                    if(!first) first = last, second = root;
                    else second = root;
                }
                last = root;
                root = root->right; //遍历右子树
            } else { //有左子树，判断root的前驱
                auto p = root->left;
                while(p -> right && p->right != root) p = p->right;
                if(!p->right) { //前驱没有指向 root
                    p->right = root;
                    root = root->left; //左子树没有遍历过 去遍历一下
                } else {
                    p->right = nullptr; //左子树遍历过了，删掉指向 root 的前驱
                    if(last && last->val > root->val){ //出现逆序对
                        if(!first) first = last, second = root;
                        else second = root;
                    }
                    last = root;
                    root = root->right; //遍历右子树 
                }
            }
        } 
        swap(first->val, second->val);
    }
};

//morris 遍历 时间 O(N) 空间 O(1)