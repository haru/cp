class Solution {
public:
    int trap(vector<int> &height) {
        int n = height.size(), pre_max[n], suf_max[n];
        pre_max[0] = height[0];
        for (int i = 1; i < n; ++i)
            pre_max[i] = max(pre_max[i - 1], height[i]);

        suf_max[n - 1] = height[n - 1];
        for (int i = n - 2; i >= 0; --i)
            suf_max[i] = max(suf_max[i + 1], height[i]);

        int ans = 0;
        for (int i = 0; i < n; ++i)
            ans += min(pre_max[i], suf_max[i]) - height[i];
        return ans;
    }
};

//前后缀分解 时间 O(n), 空间 O(n)
//https://leetcode.cn/problems/trapping-rain-water/solution/zuo-liao-nbian-huan-bu-hui-yi-ge-shi-pin-ukwm/