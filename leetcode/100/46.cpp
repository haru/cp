class Solution {
public:
    vector<vector<int>> ans;
    vector<int> path;
    vector<bool> used;

    vector<vector<int>> permute(vector<int>& nums) {
        path.resize(nums.size());
        used.resize(nums.size());
        dfs(nums, 0); 
        return ans;
    }

    void dfs(vector<int> & nums, int u) {
        if(u == nums.size())  {
            ans.push_back(path);
            return;
        }

        for(int i = 0; i < nums.size(); i++)
            if(!used[i]) {
                path[u] = nums[i];
                used[i] = true;
                dfs(nums, u + 1);
                used[i] = false; //path[u] 会被直接覆盖 不用恢复现场
            }        
    }
};