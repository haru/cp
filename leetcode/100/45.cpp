class Solution {
public:
    int jump(vector<int>& nums) {
        int n = nums.size();
        vector<int> f(n);
        //f[i] 跳到 i 所需的最小步数

        for(int i = 1, j = 0; i < n; i++){
            while(j + nums[j] < i) j++;
            f[i] = f[j] + 1; // j 跳到 i 还需要 1 步
        }
        return f[n-1];
    }
};

//从左到右，跳到每个点的最小需要步数一定是递增的
//复杂度 O(N)
