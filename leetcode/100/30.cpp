class Solution {
public:
    vector<int> findSubstring(string s, vector<string>& words) {
        vector<int> res;
        if(words.empty()) return res;
        int n = s.size(), m = words.size(), w = words[0].size();
        unordered_map<string, int> tot;
        for(auto& word : words) tot[word]++; 


        for(int i = 0; i < w; i++) {
            unordered_map<string, int> wd;
            int cnt = 0;
            for(int j = i; j + w <= n; j+= w){
                if(j >= i + m * w) { //窗口长度长到了最大，窗口开头开始往后移动
                    auto word = s.substr(j - m * w, w);
                    wd[word] --;
                    if(wd[word] < tot[word]) cnt --;
                }
                auto word = s.substr(j, w);
                wd[word] ++;
                if(wd[word] <= tot[word]) cnt++;
                if(cnt == m) res.push_back(j - (m - 1) * w); // j___ 此时j后面才是待加入的单词
            }
        }
        return res;
    }
};
//暴力思路就是长度为 m * w 的区间每个都扫描一遍，然后扫的时候用哈希表判断一下是不是
//但是暴力会超时

//优化的思路就是滑动窗口, 每次滑动一个单词的长度
//这 m * w 的区间里面的字符串都是题目里面有的就行了
//提前字符串哈希预处理一下可以到 O(N)
//直接暴力插入哈希表是 O(N*W)的
