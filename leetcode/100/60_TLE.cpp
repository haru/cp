class Solution {
public:
    vector<vector<int>> ans;
    vector<int> path;
    vector<bool> used;

    int fac(int n){
        int res = 1;
        for(int i = 1; i <= n; i++) res *= i;
        return res;
    }

    string getPermutation(int n, int k) {
        // ans = vector<vector<int>>(fac(n), vector<int>(n));
        // cout << fac(n) << endl;
        vector<int> nums(n);
        for(int i = 1; i <= n; i++) nums[i-1] = i;
        path.resize(nums.size());
        used.resize(nums.size());
        dfs(nums, 0); 
        auto arr = ans[k-1];
        string res;
        for(auto t : arr) res += to_string(t);

        // for(auto t : ans) {
        //     for(auto u : t) cout << u << ' ';
        //     cout << endl;
        // }
        return res;
    }

    void dfs(vector<int> & nums, int u) {
        if(u == nums.size())  {
            ans.push_back(path);
            return;
        }

        for(int i = 0; i < nums.size(); i++)
            if(!used[i]) {
                path[u] = nums[i];
                used[i] = true;
                dfs(nums, u + 1);
                used[i] = false; //path[u] 会被直接覆盖 不用恢复现场
            }        
    }
};