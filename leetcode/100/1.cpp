class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        map<int, int> mp;
        for(int i = 0; i< nums.size(); i++) {
        	mp.insert({nums[i], i});	
        }

        vector<int> res;

        for(int i = 0; i< nums.size(); i++) {
        	if(mp.count(target - nums[i]) != 0 &&
        		mp[target-nums[i]] != i) {
        		res.push_back(i);
        		res.push_back(mp[target-nums[i]]);
        		break;
        	}
        }
    	return res;
    }
};