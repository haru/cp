class Solution {
public:
    vector<vector<int>> insert(vector<vector<int>>& a, vector<int>& b) {
        a.push_back(b);
        sort(a.begin(), a.end());
        vector<vector<int>> res;

        int l = a[0][0], r = a[0][1];
        for(int i = 1; i < a.size(); i++) {
            if(a[i][0] > r) {
                res.push_back({l, r});
                l = a[i][0], r= a[i][1];
            }else {
                r = max(r, a[i][1]);
            }
        } 
        res.push_back({l, r});
        return res;
    }
};