class Solution {
public:
    string simplifyPath(string path) {
        string res, name;
        if(path.back()!='/') path += '/';

        for(auto c : path) {
            if(c != '/') name += c;
            else { // /xx/../ 遇到yy后面这个 / 了, name: ".."
                if(name == "..") {
                    while(res.size() && res.back() != '/') res.pop_back(); // res: /xx -> /
                    if(res.size()) res.pop_back(); //res: / -> null
                } else if(name != "." && name != "") { // "//", "." 直接忽略，不用放入 res 结果
                    res += '/' + name;
                }
                name.clear();
            }
        } 
        if(res.empty()) res = "/";

        return res;
    }
};

//这题思维感觉还是有点复杂的，要分清楚 res 和 path 的结尾
//res结尾始终都没有 "/"