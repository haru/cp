class Solution {
public:
    void sortColors(vector<int>& nums) {
        for(int i = 0, j = 0, k = nums.size() - 1; i <= k; ) { // i j 都必须下标 0开始
            if(nums[i] == 0) swap(nums[i++], nums[j++]);
            else if(nums[i] == 2) swap(nums[i], nums[k--]);
            else i++;
        } 
    }
};

//三项切分的快排
// nums[0~j-1] : 0
// nums[j~i-1] : 1
// nums[k + 1 ~ end]: 2