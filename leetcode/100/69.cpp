class Solution {
public:
    int mySqrt(int x) {
        using ll = long long;
        ll l = 0, r = x;

        while(l < r) {
            ll mid = l + r + 1 >> 1;
            if(mid * mid <= x) l = mid;
            else r = mid - 1;
        }

        return r;
    }
};

//二分