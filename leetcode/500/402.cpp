class Solution {
public:
    string removeKdigits(string num, int k) {
        k = min(k, (int)(num.size()));
        string res;
        for(auto c : num) {
            while(k && res.size() && res.back() > c) {
                res.pop_back();
                k--;
            }
            res += c;
        }
        while(k --) res.pop_back();

        k = 0;
        while(k < res.size() && res[k] == '0') k++;
        if(k == res.size()) res += '0';
        return res.substr(k);
    }
};
//贪心 字典序最小
//删掉k个后，最前面的一部分肯定是单调递增的