class Solution {
public:
    int minMoves2(vector<int>& nums) {
        int n = nums.size();
        sort(nums.begin(), nums.end());
        int res = 0;
        for(int i = 0; i < n; i++) {
            res += abs(nums[i] - nums[i / 2]); // nums[i] - nums[n/2] 一样的这两个式子恒等
        }
        return res;
    }
};

//nth_element 快速选择可以做到 O(n) 不用排序