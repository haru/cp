class Solution {
public:
    string frequencySort(string s) {
        unordered_map<char, int> cnt;
        for(auto &c : s) cnt[c] ++;       
        sort(s.begin(), s.end(), [&](char a, char b){
            if(cnt[a] != cnt[b]) return cnt[a] > cnt[b];
            return a < b;
        });
        return s;
    }
};

//双关键字排序即可