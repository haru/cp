class Solution {
public:
    int countSegments(string s) {
        int res = 0;
        for(int i = 0; i < s.size(); i++) {
            if(s[i] == ' ') continue;
            int j = i + 1;
            while(j < s.size() && s[j] != ' ') j++;
            res++;
            i = j - 1;
        }
        return res;
    }
};