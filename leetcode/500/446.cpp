#define LL long long

class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& nums) {
        const int n = nums.size();
        vector<unordered_map<LL, int>> f(n);

        int ans = 0;
        for (int i = 1; i < n; i++)
            for (int j = 0; j < i; j++) {
                LL d = (LL)(nums[i]) - nums[j];
                int sum = 0;
                if (f[j].find(d) != f[j].end())
                    sum = f[j][d];

                f[i][d] += sum + 1;
                ans += sum + 1;
            }

        return ans - n * (n - 1) / 2;
    }
};
//先算长度最少为2的总数，最后再减去长度2的所有
//目的是计算当前结尾可以接在前面的一些长度为2的上面，就凑成了长度为3的