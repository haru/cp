class Solution {
public:
    int minMoves(vector<int>& nums) {
        int minv = INT_MAX;
        for(auto c : nums) minv = min(minv, c);
        int res  = 0;
        for(auto x : nums)  res += x - minv;
        return res;
    }
};

//逆向考虑，就是每次将一个数减去1