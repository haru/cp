class Solution {
public:
    vector<int> add(vector<int> & a, vector<int>& b) {
        vector<int> c;
        for(int i = 0, t = 0; i < a.size() || i < b.size() || t; i++) {
            if(i < a.size()) t += a[i];
            if(i < b.size()) t += b[i];
            c.push_back(t % 10);
            t /= 10;
        }
        return c;
    }

    string addStrings(string num1, string num2) {
        vector<int> a, b;
        for(int i = num1.size() - 1; i >= 0; i--) a.push_back(num1[i] - '0');
        for(int i = num2.size() - 1; i >= 0; i--) b.push_back(num2[i] - '0');
        auto c = add(a, b);
        string res;
        for(int i = c.size() - 1; i >= 0; i--) res += to_string(c[i]);
        return res;       
    }
};