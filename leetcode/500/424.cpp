class Solution {
public:
    int characterReplacement(string s, int k) {
        int res = 0;
        for(char c = 'A'; c <= 'Z'; c++) {
            for(int i = 0, j = 0, cnt = 0; i < s.size(); i++) {
                if(s[i] == c) cnt++;
                while(j < i && i - j + 1 - cnt > k) {
                    if(s[j] == c) cnt --;
                    j++;
                }
                res = max(res, i - j + 1);
            }
        }
        return res;
    }
};

//双指针
//枚举每个字符在窗口出现的次数
//因为最终的答案肯定是某个字符连续一段，所以可以枚举字符