class Solution {
public:
    string smallestGoodBase(string number) {
        using ll = long long;
        ll n = stoll(number); 
        for(int t = log2(n) + 1; t >= 3; t--) {
            ll k = pow(n, 1.0/(t-1));
            ll r = 0;
            for(int i = 0; i < t; i++) r = r * k + 1;
            if(r == n) return to_string(k);
        }
        return to_string(n-1); //number 是 2位数的情况
    }
};

//https://www.acwing.com/solution/content/6428/
//不等式推公式