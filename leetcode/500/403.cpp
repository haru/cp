
class Solution {
public:
    vector<int> stones;
    unordered_map<int, int> hash;
    int f[2010][2010];

    int dp(int i, int j) {
        if(f[i][j] != -1) return f[i][j];
        f[i][j] = 0;
        for(int k = max(1, j -1); k <= j + 1; k++)
            if(hash.count(stones[i] - k)) {
                int p = hash[stones[i] - k];
                if(dp(p, k)) {
                    f[i][j] = 1;
                    break;
                }
            }
        return f[i][j];
    }

    bool canCross(vector<int>& _stones) {
        memset(f, -1, sizeof f);
        f[0][1] = 1;
        stones = _stones;
        int n = stones.size();
        for(int i = 0; i < n; i++) hash[stones[i]] = i;
        for(int i = 0; i < n; i++) {
            if(dp(n-1, i)) return true;
        }
        return false; 
    }
};


//DP O(n^2) 会超时，需要写成记忆化搜索从后往前搜
//f[i][j]: 能否跳到i，而且可以从i开始往后跳j步