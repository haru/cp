// The rand7() API is already defined for you.
// int rand7();
// @return a random integer in the range 1 to 7

class Solution {
public:
    int rand10() {
      int t = (rand7() - 1) * 7 + rand7(); // 1~49
      if(t > 40) return rand10();
      return (t - 1) % 10 + 1; // [0, 10, 20, 30] [1, 11, 21, 31] 平均分成10组
    }
};