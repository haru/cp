class Solution {
public:
    ListNode* reverse(ListNode* head) {
        auto a = head, b = head->next;
        while (b) {
            auto c = b->next;
            b->next = a;
            a = b, b = c;
        }
        head->next = NULL;
        return a;
    }

    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        l1 = reverse(l1), l2 = reverse(l2);
        auto head = new ListNode(-1);
        int t = 0;
        while (l1 || l2 || t) {
            if (l1) t += l1->val, l1 = l1->next;
            if (l2) t += l2->val, l2 = l2->next;
            auto cur = new ListNode(t % 10);
            t /= 10;
            cur->next = head->next; //头插
            head->next = cur;
        }
        return head->next;
    }
};

//反转链表 + 高精度加法