class Solution {
public:
    vector<int> findRightInterval(vector<vector<int>>& q) {
        int n = q.size();
        for(int i = 0; i < n; i++) q[i].push_back(i);
        sort(q.begin(), q.end());
        vector<int> res(n, -1);

        for(auto &x: q) {
            int l = 0, r = n - 1;
            while(l < r) {
                int mid = l + r >> 1;
                if(q[mid][0] >= x[1]) r = mid;
                else l = mid + 1;
            }
            if(q[r][0] >= x[1]) res[x[2]] = q[r][2];
        }
        return res;
    }
};

//左端点排序 + 二分
//为啥我感觉直接右端点排序就能做，不用二分，和 LC435 一样的思路