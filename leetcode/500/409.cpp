class Solution {
public:
    int longestPalindrome(string s) {
        unordered_map<char, int> m;
        for(auto c : s) m[c]++;
        int res = 0;
        for(auto [a, k] : m) res += k / 2 * 2;
        if(res < s.size()) res += 1;       
        return res;
    }
};