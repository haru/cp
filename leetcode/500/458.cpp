class Solution {
public:
    int poorPigs(int n, int minutesToDie, int minutesToTest) {
 		int k = minutesToTest / minutesToDie + 1;
 		return ceil(log(n) / log(k) - 1e-5);       
    }
};

//https://www.acwing.com/solution/content/131/
//But how to prove?
//题解的版本代码过不了