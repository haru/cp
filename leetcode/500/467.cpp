class Solution {
public:
    int findSubstringInWraproundString(string s) {
        unordered_map<char, int> cnt;
        for(int i = 0; i < s.size();) {
            int j = i + 1;
            while(j < s.size() && (s[j] - s[j-1] == 1) || s[j] == 'a' &&
                s[j-1] == 'z') j++;
            while(i < j) cnt[s[i]] = max(cnt[s[i]], j - i), i++;
        }  
        int res = 0;
        for(auto [a, b]  : cnt) res += b;
        return res;
    }
};

//观察到子串可以根据开头字母进行分类
//双指针 O(n)
//https://www.acwing.com/solution/content/380/