class Solution {
public:
    int eraseOverlapIntervals(vector<vector<int>>& q) {
        sort(q.begin(), q.end(), [](vector<int> a, vector<int> b) {
            return a[1] < b[1];
        });

        int res = 1, r = q[0][1];
        for(int i = 1; i < q.size(); i++) {
            if(q[i][0] >= r) {
                res++;
                r = q[i][1];
            }
        }
        return q.size() - res;
    }
};

//贪心
//右端点排序 挨个能选则选