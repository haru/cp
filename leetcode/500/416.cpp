class Solution {
public:
    bool canPartition(vector<int>& nums) {
        bitset<10001> f;
        f[0] = 1; //最右边的最低位
        int sum = 0;
        for(auto c : nums){
            f |= f << c; //移到高位
            sum += c;
        }
        if(sum % 2) return false;
        return f[sum / 2];
    }
};

//01背包 bitset 优化