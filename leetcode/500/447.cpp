class Solution {
public:
    int numberOfBoomerangs(vector<vector<int>>& p) {
        int res = 0;
        for (int i = 0; i < p.size(); i ++ ) {
            unordered_map<int, int> cnt;
            for (int j = 0; j < p.size(); j ++ )
                if (i != j) {
                    int dx = p[i][0] - p[j][0];
                    int dy = p[i][1] - p[j][1];
                    int dist = dx * dx + dy * dy;
                    cnt[dist] ++ ;
                }
            for (auto [d, c]: cnt) res += c * (c - 1);
        }
        return res;
    }
};
//枚举i, 然后哈希表记录所有到i点距离，排列A(cnt, 2) 即可