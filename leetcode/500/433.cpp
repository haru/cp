class Solution {
public:
    int minMutation(string start, string end, vector<string>& bank) {
        unordered_set<string> S;
        for (auto& s: bank) S.insert(s);
        unordered_map<string, int> dist;
        queue<string> q;
        q.push(start);
        dist[start] = 0;
        char chrs[4] = {'A', 'T', 'C', 'G'};

        while (q.size()) {
            auto t = q.front();
            q.pop();
            for (int i = 0; i < t.size(); i ++ ) {
                auto s = t;
                for (char c: chrs) {
                    s[i] = c;
                    if (S.count(s) && dist.count(s) == 0) {
                        dist[s] = dist[t] + 1;
                        if (s == end) return dist[s];
                        q.push(s);
                    }
                }
            }
        }
        return -1;
    }
};

//BFS 求最短路