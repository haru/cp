#!/bin/zsh

trim() {
    local var="$*"
    var="${var#"${var%%[![:space:]]*}"}"   
    var="${var%"${var##*[![:space:]]}"}" 
    echo -n "$var"
}

count=$(find . \( -name '*.cpp' -o -name '*.cc' -o -name '*.java' -o -name '*.js' -o -name '*.py' \) | wc -l)
count=$(trim "$count")
time=$(date)

echo "$count problems solved as of $time"

